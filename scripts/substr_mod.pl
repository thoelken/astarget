#!/usr/bin/perl

use strict;
use warnings "all";

my $seq = "";
my $head = "";

my @lengths;

foreach (@ARGV) {
	if ($_ =~ /^([0-9]+)$/) {push(@lengths,$1);}
}

# Collect data
while (<STDIN>) {
	chomp;
	if ($_ =~ />/) {
		if ($head ne "") {die("Only one fasta entry can be run!")}
		$head = $_;
	}
	else {
		$seq .= $_;
	}
}

#$seq = reverse($seq);
$seq =~ tr/ACTG/TGAC/;

my %hash;
foreach my $sub (@lengths) {
	# generate all rv_substrings of length $sub
	for (my $i = 0; $i < length($seq)-$sub; $i++) {
		my $subseq = reverse(substr($seq,$i,$sub));
		$hash{$subseq} .= "|$i";
	}
}

foreach my $subseq (sort keys %hash) {
		my @pos = split(/\|/,$hash{$subseq}); shift @pos;
		print ">".join(",",@pos)."-".scalar(@pos)."x\n";
		print "$subseq\n";
}	
