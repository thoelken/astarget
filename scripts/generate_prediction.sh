#!/bin/sh -e

if [ "$#" -ne 7 ]; then
    echo "Illegal number of parameters"
    echo "usage: $0 <scripts-dir> <input> <output> <output-filtered> <length> <gc-ratio> <max-mononuc-stretches>"
    exit 2
fi

SCRIPTS_DIR="${1}"
INPUT="${2}"
OUTPUT="${3}"
OUTPUT_FILTERED="${4}"
LENGTH="${5}"
GC="${6}"
MAX_MONONUC_STRETCHES="${7}"

## Generate data
"${SCRIPTS_DIR}/oligoBuilder.pl" "-length=${LENGTH}" "-scripts=${SCRIPTS_DIR}" "${INPUT}"

## Score sources
"${SCRIPTS_DIR}/scorer.pl" "${INPUT}" "${LENGTH}" | sort -k1,1n > "${OUTPUT}"

## Filter
awk "{if (\$5 >= ${GC} && \$4 < ${MAX_MONONUC_STRETCHES}) {print \$0}}" "${OUTPUT}" > "${OUTPUT_FILTERED}"

exit 0
