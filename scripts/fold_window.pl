#!/usr/bin/perl

use strict;
use warnings "all";

our $fold_window = 300;	# Window to fold big RNAs

my @globalfold;
my ($name,$seq) = &parse_RNAfold($ARGV[0]);
my $overlap = int($fold_window/3);
for (my $i = 0; $i <= length($seq)-$overlap; $i += $overlap) {
	my $subseq = substr($seq,$i,$fold_window);
	open(FILE, ">/tmp/$$.fasta") || die("Could not open file '/tmp/$$.fasta': $!");
	print FILE ">SUBSEQ_$i\n$subseq\n";
	close(FILE);

	system("RNAfold </tmp/$$.fasta >/tmp/$$.fold");
	my ($sname,$sseq,$sfold) = &parse_RNAfold("/tmp/$$.fold");
	unlink("/tmp/$$.fold");

	my @fold = split(//,$sfold);
	for (my $k = 0; $k < @fold; $k++) {
		if (!defined($globalfold[$i+$k]) || $globalfold[$i+$k] eq ".") {
			$globalfold[$i+$k] = $fold[$k];
		}
	}
}

print ">$name\n".join("",@globalfold)." $fold_window\n";


sub parse_RNAfold {
	my $fa_name = "Unknown";
	my $fold = "";
	my $seq = "";
	open(FILE, "<$_[0]") || die("Could not open file '$_[0]': $!");
	while (<FILE>) {
		chomp;
		if ($_ =~ /^>([^ ]+)/) 		{$fa_name = $1;}
		elsif ($_ =~ /^([\.\(\)]+)\s/) 	{$fold = $1;}
		else {
			$seq .= $_;
		}
	}
	close(FILE);

	return ($fa_name,$seq,$fold);
}

