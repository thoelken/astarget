<?php declare(strict_types = 1);

namespace Tests\AppBundle\Service;

use AppBundle\Service\FastaManager;

/**
 * @SuppressWarnings(PMD.TooManyPublicMethods)
 */
class FastaManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FastaManager
     */
    private $_object;

    /**
     * @var \AppBundle\Service\Filesystem
     */
    private $_filesystem;

    /**
     * @var \AppBundle\Util\Path
     */
    private $_path;

    /**
     * @var int
     */
    private $_maxFastaLength;

    public function setUp() : void
    {
        parent::setUp();

        $this->_filesystem = $this
            ->getMockBuilder(\AppBundle\Service\Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path = $this
            ->getMockBuilder(\AppBundle\Util\Path::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_maxFastaLength = 50;

        $this->_object = new FastaManager(
            $this->_filesystem,
            $this->_path,
            $this->_maxFastaLength
        );
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'FASTA_OK'       => 0,
                'FASTA_INVALID'  => 1,
                'FASTA_TOO_LONG' => 2
            ],
            $object->getConstants()
        );
    }

    public function testGetFormFastaFromTextArea() : void
    {
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $checkboxMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $textareaMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resultString = 'ACGT';

        $form
            ->expects($this->at(0))
            ->method('get')
            ->with($this->equalTo('uploadfilechoice'))
            ->will($this->returnValue($checkboxMock));

        $checkboxMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue(false));

        $form
            ->expects($this->at(1))
            ->method('get')
            ->with($this->equalTo('fasta'))
            ->will($this->returnValue($textareaMock));

        $textareaMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue($resultString));

        $this->assertEquals($resultString, $this->_object->getFormFasta($form));
    }

    public function testGetFormFastaFromFile() : void
    {
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $checkboxMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uploadFileMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uploadFileDataMock = $this
            ->getMockBuilder(\Symfony\Component\HttpFoundation\File\UploadedFile::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resultPath = "/var/tmp/test.fna";

        $resultString = 'ACGT';

        $form
            ->expects($this->at(0))
            ->method('get')
            ->with($this->equalTo('uploadfilechoice'))
            ->will($this->returnValue($checkboxMock));

        $checkboxMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue(true));

        $form
            ->expects($this->at(1))
            ->method('get')
            ->with($this->equalTo('fastafile'))
            ->will($this->returnValue($uploadFileMock));

        $uploadFileMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue($uploadFileDataMock));

        $uploadFileDataMock
            ->expects($this->once())
            ->method('getPathname')
            ->will($this->returnValue($resultPath));

        $this->_filesystem
            ->expects($this->once())
            ->method('readFile')
            ->with($this->equalTo($resultPath))
            ->will($this->returnValue($resultString));

        $this->assertEquals($resultString, $this->_object->getFormFasta($form));
    }

    public function testGetFormFastaFail() : void
    {
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $checkboxMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uploadFileMock = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();

        $form
            ->expects($this->at(0))
            ->method('get')
            ->with($this->equalTo('uploadfilechoice'))
            ->will($this->returnValue($checkboxMock));

        $checkboxMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue(true));

        $form
            ->expects($this->at(1))
            ->method('get')
            ->with($this->equalTo('fastafile'))
            ->will($this->returnValue($uploadFileMock));

        $uploadFileMock
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue(null));

        $this->assertNull($this->_object->getFormFasta($form));
    }

    public function testWriteFastaToFile() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resultPath = '/var/tmp/test.fna';
        $inputFasta = 'ACGT';

        $this->_filesystem
            ->expects($this->at(0))
            ->method('addJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue(true));

        $this->_path
            ->expects($this->once())
            ->method('getFilePath')
            ->with(
                $this->equalTo($job),
                $this->equalTo(\AppBundle\Util\Path::FILENAME_INPUT)
            )
            ->will($this->returnValue($resultPath));

        $this->_filesystem
            ->expects($this->at(1))
            ->method('writeFile')
            ->with($this->equalTo($resultPath), $this->equalTo($inputFasta))
            ->will($this->returnValue(true));

        $this->assertTrue($this->_object->writeFastaToFile($inputFasta, $job));
    }

    public function testWriteFastaToFileFail() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $inputFasta = 'ACGT';

        $this->_filesystem
            ->expects($this->at(0))
            ->method('addJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue(false));

        $this->assertFalse($this->_object->writeFastaToFile($inputFasta, $job));
    }

    public function testCorrectInput() : void
    {
        $testsamples   = ["ACGT", "ACGU", "ACGT\rACGT", "ACGT\r\nACGU", "ACGT\nACGT"];
        $testcorrected = ["ACGT\n", "ACGT\n", "ACGTACGT\n",   "ACGTACGT\n",     "ACGTACGT\n"];

        $testcomment = ";Random Comment\r\nACGTCGUAC";
        $testcommentcorrected = ";Random Comment\nACGTCGTAC\n";

        $this->assertEquals($testcorrected[0], $this->_object->correctInput($testsamples[0]));
        $this->assertEquals($testcorrected[1], $this->_object->correctInput($testsamples[1]));
        $this->assertEquals($testcorrected[2], $this->_object->correctInput($testsamples[2]));
        $this->assertEquals($testcorrected[3], $this->_object->correctInput($testsamples[3]));
        $this->assertEquals($testcorrected[4], $this->_object->correctInput($testsamples[4]));
        $this->assertEquals(
            $testcommentcorrected,
            $this->_object->correctInput($testcomment)
        );
    }

    public function testGetFastaLength() : void
    {
        $test = ["ACGT\n", "ACGTACGT\n", "ACGTACGT"];
        $testcomment = ";Random Comment\nACGTCGTAC\n";

        $this->assertEquals(4, $this->_object->getFastaLength($test[0]));
        $this->assertEquals(8, $this->_object->getFastaLength($test[1]));
        $this->assertEquals(8, $this->_object->getFastaLength($test[2]));
        $this->assertEquals(9, $this->_object->getFastaLength($testcomment));
    }

    public function testGetFastaLengthInvalid() : void
    {
        $this->assertEquals(0, $this->_object->getFastaLength(">COMMENT"));
    }

    public function testCheckFastaEmpty() : void
    {
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta("")
        );
    }

    public function testCheckFastaOneLine() : void
    {
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta(">SomeHeadLine")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta(";ACGTACGT")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta("ACGT")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_TOO_LONG,
            $this->_object->checkFasta("ACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT")
        );
    }

    public function testCheckFastaMoreLines() : void
    {
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta(">SomeHeadLine\n;SomeComment")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_OK,
            $this->_object->checkFasta(">SomeHeadLine\nACGATACGT")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta(";SomeComment\nACACGGACT")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta(";Some\n;Comment\nACACAGT")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_INVALID,
            $this->_object->checkFasta("ACACGT\nACACGTA")
        );
        $this->assertEquals(
            \AppBundle\Service\FastaManager::FASTA_TOO_LONG,
            $this->_object->checkFasta(">HL\n;C\nACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGTACGT")
        );
    }
}
