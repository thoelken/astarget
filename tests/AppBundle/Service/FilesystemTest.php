<?php declare(strict_types = 1);

namespace Tests\AppBundle\Service;

use AppBundle\Service\Filesystem;

/**
 * @SuppressWarnings(PMD.TooManyPublicMethods)
 */
class FilesystemTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Filesystem
     */
    private $_object;

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $_filesystem;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    /**
     * @var \AppBundle\Util\FileReader
     */
    private $_filereader;

    /**
     * @var \AppBundle\Util\Path
     */
    private $_path;

    public function setUp() : void
    {
        parent::setUp();

        $this->_filesystem = $this
            ->getMockBuilder(\Symfony\Component\Filesystem\Filesystem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_logger = $this
            ->getMockBuilder(\Psr\Log\LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_filereader = $this
            ->getMockBuilder(\AppBundle\Util\FileReader::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path = $this
            ->getMockBuilder(\AppBundle\Util\Path::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new Filesystem(
            $this->_filesystem,
            $this->_logger,
            $this->_filereader,
            $this->_path
        );
    }

    public function testAddJobDirectory() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('mkdir')
            ->with($this->equalTo('job/directory'));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        $this->assertTrue($this->_object->addJobDirectory($job));
    }

    public function testAddJobDirectoryWhenAddingFailed() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $exception = $this
            ->getMockBuilder(\Symfony\Component\Filesystem\Exception\IOException::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('mkdir')
            ->with($this->equalTo('job/directory'))
            ->will($this->throwException($exception));

        $exception
            ->expects($this->once())
            ->method('getPath')
            ->will($this->returnValue('path/exception'));

        $this->_logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->equalTo('An error occurred while adding a directory at path/exception'),
                $this->equalTo(['exception' => $exception])
            );

        $this->assertFalse($this->_object->addJobDirectory($job));
    }

    public function testAddJobDirectoryWithUnexpectedException() : void
    {
        $exception         = null;
        $expectedException = new \Exception();

        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('mkdir')
            ->with($this->equalTo('job/directory'))
            ->will($this->throwException($expectedException));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        try {
            $this->_object->addJobDirectory($job);
        } catch (\Exception $ex) {
            $exception = $ex;
        }

        $this->assertNotNull($exception);
        $this->assertEquals($expectedException, $exception);
    }

    public function testRemoveJobDirectory() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo('job/directory'));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        $this->assertTrue($this->_object->removeJobDirectory($job));
    }

    public function testRemoveJobDirectoryWhenRemoveFailed() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $exception = $this
            ->getMockBuilder(\Symfony\Component\Filesystem\Exception\IOException::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo('job/directory'))
            ->will($this->throwException($exception));

        $exception
            ->expects($this->once())
            ->method('getPath')
            ->will($this->returnValue('path/exception'));

        $this->_logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->equalTo('An error occurred while removing a directory at path/exception'),
                $this->equalTo(['exception' => $exception])
            );

        $this->assertFalse($this->_object->removeJobDirectory($job));
    }

    public function testRemoveJobDirectoryWithUnexpectedException() : void
    {
        $exception         = null;
        $expectedException = new \Exception();

        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_path
            ->expects($this->once())
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/directory'));

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo('job/directory'))
            ->will($this->throwException($expectedException));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        try {
            $this->_object->removeJobDirectory($job);
        } catch (\Exception $ex) {
            $exception = $ex;
        }

        $this->assertNotNull($exception);
        $this->assertEquals($expectedException, $exception);
    }

    public function testWriteFile() : void
    {
        $path = 'var/data/5/test.fna';
        $content = 'bla';

        $this->_filesystem
            ->expects($this->once())
            ->method('dumpFile')
            ->with($this->equalTo($path), $this->equalTo($content));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        $this->assertTrue($this->_object->writeFile($path, $content));
    }

    public function testWriteFileWhenWriteFailed() : void
    {
        $path = 'var/data/5/test.fna';
        $content = 'bla';
        $exception = $this
            ->getMockBuilder(\Symfony\Component\Filesystem\Exception\IOException::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_filesystem
            ->expects($this->once())
            ->method('dumpFile')
            ->with($this->equalTo($path), $this->equalTo($content))
            ->will($this->throwException($exception));

        $exception
            ->expects($this->once())
            ->method('getPath')
            ->will($this->returnValue('path/exception'));

        $this->_logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->equalTo('An error occurred while creating a file at path/exception'),
                $this->equalTo(['exception' => $exception])
            );

        $this->assertFalse($this->_object->writeFile($path, $content));
    }

    public function testWriteFileWithUnexpectedException() : void
    {
        $path = 'var/data/5/test.fna';
        $content = 'bla';
        $exception         = null;
        $expectedException = new \Exception();

        $this->_filesystem
            ->expects($this->once())
            ->method('dumpFile')
            ->with($this->equalTo($path), $this->equalTo($content))
            ->will($this->throwException($expectedException));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        try {
            $this->_object->writeFile($path, $content);
        } catch (\Exception $ex) {
            $exception = $ex;
        }

        $this->assertNotNull($exception);
        $this->assertEquals($expectedException, $exception);
    }

    public function testRemoveFile() : void
    {
        $path = 'var/data/5/test.fna';

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($path));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        $this->assertTrue($this->_object->removeFile($path));
    }

    public function testRemoveFileWhenRemoveFailed() : void
    {
        $path = 'var/data/5/test.fna';
        $exception = $this
            ->getMockBuilder(\Symfony\Component\Filesystem\Exception\IOException::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($path))
            ->will($this->throwException($exception));

        $exception
            ->expects($this->once())
            ->method('getPath')
            ->will($this->returnValue('path/exception'));

        $this->_logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->equalTo('An error occurred while removing a file at path/exception'),
                $this->equalTo(['exception' => $exception])
            );

        $this->assertFalse($this->_object->removeFile($path));
    }

    public function testRemoveFileWithUnexpectedException() : void
    {
        $path = 'var/data/5/test.fna';
        $exception         = null;
        $expectedException = new \Exception();

        $this->_filesystem
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($path))
            ->will($this->throwException($expectedException));

        $this->_logger
            ->expects($this->never())
            ->method('error');

        try {
            $this->_object->removeFile($path);
        } catch (\Exception $ex) {
            $exception = $ex;
        }

        $this->assertNotNull($exception);
        $this->assertEquals($expectedException, $exception);
    }

    public function testReadFileWhenReadFailed() : void
    {
        $path = 'var/data/not-existing-file.fna';

        $this->_filereader
            ->expects($this->once())
            ->method('readFile')
            ->with($this->equalTo($path));

        $this->assertNull($this->_object->readFile($path));
    }
}
