<?php declare(strict_types = 1);

namespace Tests\AppBundle\Service;

use AppBundle\Service\Process;

/**
 * @SuppressWarnings(PMD.ExcessiveMethodLength)
 */
class ProcessTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Process
     */
    private $_object;

    /**
     * @var \AppBundle\Util\Path
     */
    private $_path;

    /**
     * @var \AppBundle\Util\ProcessBuilderFactory
     */
    private $_processBuilderFactory;

    /**
     * @var \AppBundle\Util\JobTimeout
     */
    private $_jobTimeoutUtil;

    public function setUp() : void
    {
        parent::setUp();

        $this->_path = $this
            ->getMockBuilder(\AppBundle\Util\Path::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_processBuilderFactory = $this
            ->getMockBuilder(\AppBundle\Util\ProcessBuilderFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_jobTimeoutUtil = $this
            ->getMockBuilder(\AppBundle\Util\JobTimeout::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new Process($this->_path, $this->_processBuilderFactory, $this->_jobTimeoutUtil);
    }

    public function testGeneratePrediction() : void
    {
        $processBuilder = $this
            ->getMockBuilder(\Symfony\Component\Process\ProcessBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $process = $this
            ->getMockBuilder(\Symfony\Component\Process\Process::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_processBuilderFactory
            ->expects($this->once())
            ->method('getProcessBuilder')
            ->will($this->returnValue($processBuilder));

        $job
            ->expects($this->once())
            ->method('getLength')
            ->will($this->returnValue(15));
        $job
            ->expects($this->once())
            ->method('getGcRatio')
            ->will($this->returnValue(40));
        $job
            ->expects($this->once())
            ->method('getMaxMononucStretches')
            ->will($this->returnValue(3));

        $this->_path
            ->expects($this->at(0))
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/dir'));
        $this->_path
            ->expects($this->at(1))
            ->method('getScriptPath')
            ->with($this->equalTo('generate_prediction.sh'))
            ->will($this->returnValue('script/dir/generator'));
        $this->_path
            ->expects($this->at(2))
            ->method('getScriptsDirectory')
            ->will($this->returnValue('script/dir'));
        $this->_path
            ->expects($this->at(3))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1.fna'))
            ->will($this->returnValue('job/dir/input'));
        $this->_path
            ->expects($this->at(4))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1.csv'))
            ->will($this->returnValue('job/dir/output'));
        $this->_path
            ->expects($this->at(5))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1_filtered.csv'))
            ->will($this->returnValue('job/dir/output-filtered'));

        $this->_jobTimeoutUtil
            ->expects($this->once())
            ->method('getTimeoutInSeconds')
            ->with($this->equalTo($job))
            ->will($this->returnValue(1234));

        $processBuilder
            ->expects($this->at(0))
            ->method('setWorkingDirectory')
            ->with($this->equalTo('job/dir'));
        $processBuilder
            ->expects($this->at(1))
            ->method('setPrefix')
            ->with($this->equalTo('script/dir/generator'));
        $processBuilder
            ->expects($this->at(2))
            ->method('setArguments')
            ->with(
                $this->equalTo(
                    [
                        'script/dir',
                        'job/dir/input',
                        'job/dir/output',
                        'job/dir/output-filtered',
                        15,
                        40,
                        3,
                    ]
                )
            );
        $processBuilder
            ->expects($this->at(3))
            ->method('setTimeout')
            ->with($this->equalTo(1234));
        $processBuilder
            ->expects($this->at(4))
            ->method('getProcess')
            ->will($this->returnValue($process));

        $process
            ->expects($this->once())
            ->method('run');

        $this->assertEquals($process, $this->_object->generatePrediction($job));
    }

    public function testGeneratePredictionWithTimeout() : void
    {
        $processBuilder = $this
            ->getMockBuilder(\Symfony\Component\Process\ProcessBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $process = $this
            ->getMockBuilder(\Symfony\Component\Process\Process::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $exception = $this
            ->getMockBuilder(\Symfony\Component\Process\Exception\ProcessTimedOutException::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_processBuilderFactory
            ->expects($this->once())
            ->method('getProcessBuilder')
            ->will($this->returnValue($processBuilder));

        $job
            ->expects($this->once())
            ->method('getLength')
            ->will($this->returnValue(15));
        $job
            ->expects($this->once())
            ->method('getGcRatio')
            ->will($this->returnValue(40));
        $job
            ->expects($this->once())
            ->method('getMaxMononucStretches')
            ->will($this->returnValue(3));

        $this->_path
            ->expects($this->at(0))
            ->method('getJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue('job/dir'));
        $this->_path
            ->expects($this->at(1))
            ->method('getScriptPath')
            ->with($this->equalTo('generate_prediction.sh'))
            ->will($this->returnValue('script/dir/generator'));
        $this->_path
            ->expects($this->at(2))
            ->method('getScriptsDirectory')
            ->will($this->returnValue('script/dir'));
        $this->_path
            ->expects($this->at(3))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1.fna'))
            ->will($this->returnValue('job/dir/input'));
        $this->_path
            ->expects($this->at(4))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1.csv'))
            ->will($this->returnValue('job/dir/output'));
        $this->_path
            ->expects($this->at(5))
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('hyp_up1_filtered.csv'))
            ->will($this->returnValue('job/dir/output-filtered'));

        $this->_jobTimeoutUtil
            ->expects($this->once())
            ->method('getTimeoutInSeconds')
            ->with($this->equalTo($job))
            ->will($this->returnValue(1234));

        $processBuilder
            ->expects($this->at(0))
            ->method('setWorkingDirectory')
            ->with($this->equalTo('job/dir'));
        $processBuilder
            ->expects($this->at(1))
            ->method('setPrefix')
            ->with($this->equalTo('script/dir/generator'));
        $processBuilder
            ->expects($this->at(2))
            ->method('setArguments')
            ->with(
                $this->equalTo(
                    [
                        'script/dir',
                        'job/dir/input',
                        'job/dir/output',
                        'job/dir/output-filtered',
                        15,
                        40,
                        3,
                    ]
                )
            );
        $processBuilder
            ->expects($this->at(3))
            ->method('setTimeout')
            ->with($this->equalTo(1234));
        $processBuilder
            ->expects($this->at(4))
            ->method('getProcess')
            ->will($this->returnValue($process));

        $process
            ->expects($this->once())
            ->method('run')
            ->will($this->throwException($exception));

        $this->assertNull($this->_object->generatePrediction($job));
    }
}
