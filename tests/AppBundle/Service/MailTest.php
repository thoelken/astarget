<?php declare(strict_types = 1);

namespace Tests\AppBundle\Service;

use AppBundle\Service\Mail;

class MailTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Mail
     */
    private $_object;

    /**
     * @var \Swift_Mailer
     */
    private $_mailer;

    /**
     * @var \AppBundle\Util\MailMessage
     */
    private $_mailMessage;

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $_twigEngine;

    /**
     * @var string
     */
    private $_adminMailAddress = 'admin@example.org';

    /**
     * @var int
     */
    private $_deleteAfterDays = 15;

    /**
     * @var string
     */
    private $_senderMailAddress = 'sender@example.org';

    public function setUp() : void
    {
        parent::setUp();

        $this->_mailer = $this
            ->getMockBuilder(\Swift_Mailer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_mailMessage = $this
            ->getMockBuilder(\AppBundle\Util\MailMessage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_twigEngine = $this
            ->getMockBuilder(\Symfony\Bundle\TwigBundle\TwigEngine::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new Mail(
            $this->_mailer,
            $this->_mailMessage,
            $this->_twigEngine,
            $this->_adminMailAddress,
            $this->_deleteAfterDays,
            $this->_senderMailAddress
        );
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [],
            $object->getConstants()
        );
    }

    public function testSendFeedback() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $message = $this
            ->getMockBuilder(\Swift_Message::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_mailMessage
            ->expects($this->once())
            ->method('getMessage')
            ->will($this->returnValue($message));

        $job
            ->expects($this->once())
            ->method('getEmail')
            ->will($this->returnValue('to@example.org'));

        $this->_twigEngine
            ->expects($this->once())
            ->method('render')
            ->with(
                $this->equalTo('mail/feedback.txt.twig'),
                $this->equalTo(
                    [
                        'deleteAfterDays' => $this->_deleteAfterDays,
                        'job'             => $job,
                    ]
                )
            )
            ->will($this->returnValue('rendered'));

        $message
            ->expects($this->once())
            ->method('setSubject')
            ->with($this->equalTo('Feedback to Job'));
        $message
            ->expects($this->once())
            ->method('setFrom')
            ->with($this->equalTo($this->_senderMailAddress));
        $message
            ->expects($this->once())
            ->method('setTo')
            ->with($this->equalTo('to@example.org'));
        $message
            ->expects($this->once())
            ->method('setBody')
            ->with(
                $this->equalTo('rendered'),
                $this->equalTo('text/plain')
            );

        $this->_mailer
            ->expects($this->once())
            ->method('send')
            ->with($this->equalTo($message));

        $this->_object->sendFeedback($job);
    }

    public function testSendNotifyJobFailed() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $message = $this
            ->getMockBuilder(\Swift_Message::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_mailMessage
            ->expects($this->once())
            ->method('getMessage')
            ->will($this->returnValue($message));

        $this->_twigEngine
            ->expects($this->once())
            ->method('render')
            ->with(
                $this->equalTo('mail/jobFailed.txt.twig'),
                $this->equalTo(
                    [
                        'errorOutput' => 'errorOutput',
                        'job'         => $job,
                    ]
                )
            )
            ->will($this->returnValue('rendered'));

        $message
            ->expects($this->once())
            ->method('setSubject')
            ->with($this->equalTo('Job failed'));
        $message
            ->expects($this->once())
            ->method('setFrom')
            ->with($this->equalTo($this->_senderMailAddress));
        $message
            ->expects($this->once())
            ->method('setTo')
            ->with($this->equalTo($this->_adminMailAddress));
        $message
            ->expects($this->once())
            ->method('setBody')
            ->with(
                $this->equalTo('rendered'),
                $this->equalTo('text/plain')
            );

        $this->_mailer
            ->expects($this->once())
            ->method('send')
            ->with($this->equalTo($message));

        $this->_object->sendNotifyJobFailed($job, 'errorOutput');
    }

    public function testSendNotifyJobTimeout() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $message = $this
            ->getMockBuilder(\Swift_Message::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_mailMessage
            ->expects($this->once())
            ->method('getMessage')
            ->will($this->returnValue($message));

        $this->_twigEngine
            ->expects($this->once())
            ->method('render')
            ->with(
                $this->equalTo('mail/jobTimeout.txt.twig'),
                $this->equalTo(['job' => $job])
            )
            ->will($this->returnValue('rendered'));

        $message
            ->expects($this->once())
            ->method('setSubject')
            ->with($this->equalTo('Job timed out'));
        $message
            ->expects($this->once())
            ->method('setFrom')
            ->with($this->equalTo($this->_senderMailAddress));
        $message
            ->expects($this->once())
            ->method('setTo')
            ->with($this->equalTo($this->_adminMailAddress));
        $message
            ->expects($this->once())
            ->method('setBody')
            ->with(
                $this->equalTo('rendered'),
                $this->equalTo('text/plain')
            );

        $this->_mailer
            ->expects($this->once())
            ->method('send')
            ->with($this->equalTo($message));

        $this->_object->sendNotifyJobTimeout($job);
    }
}
