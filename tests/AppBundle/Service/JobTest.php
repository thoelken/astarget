<?php declare(strict_types = 1);

namespace Tests\AppBundle\Service;

use AppBundle\Service\Job;

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 * @SuppressWarnings(PMD.ExcessiveMethodLength)
 * @SuppressWarnings(PMD.TooManyPublicMethods)
 */
class JobTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Job
     */
    private $_object;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $_entityManager;

    /**
     * @var \AppBundle\Util\HashGenerator
     */
    private $_hashGenerator;

    /**
     * @var \AppBundle\Util\Time
     */
    private $_time;

    private $_deleteAfterDays = 30;

    public function setUp() : void
    {
        parent::setUp();

        $this->_entityManager = $this
            ->getMockBuilder(\Doctrine\ORM\EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_hashGenerator = $this
            ->getMockBuilder(\AppBundle\Util\HashGenerator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_time = $this
            ->getMockBuilder(\AppBundle\Util\Time::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new Job($this->_entityManager, $this->_hashGenerator, $this->_time, $this->_deleteAfterDays);
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'REPOSITORY'      => 'AppBundle:Job',
                'STATUS_ACTIVE'   => 'active',
                'STATUS_EXECUTED' => 'executed',
                'STATUS_FAILED'   => 'failed',
                'STATUS_NEW'      => 'new',
                'STATUS_REMOVED'  => 'removed',
                'STATUS_TIMEOUT'  => 'timeout',
            ],
            $object->getConstants()
        );
    }

    /**
     * @dataProvider dataProviderForTestCountByStatus
     */
    public function testCountByStatus(array $return, int $expected) : void
    {
        $status = 'new';

        $repository = $this
            ->getMockBuilder(\Doctrine\ORM\EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Job'))
            ->will($this->returnValue($repository));

        $repository
            ->expects($this->once())
            ->method('findBy')
            ->with($this->equalTo(['status' => $status]))
            ->will($this->returnValue($return));

        $this->assertEquals($expected, $this->_object->countByStatus($status));
    }

    public function testCreateJob() : void
    {
        $form = $this
            ->getMockBuilder(\Symfony\Component\Form\Form::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dateTime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $form
            ->expects($this->once())
            ->method('getData')
            ->will($this->returnValue($job));

        $this->_time
            ->expects($this->once())
            ->method('getCurrentDateTime')
            ->will($this->returnValue($dateTime));

        $this->_hashGenerator
            ->expects($this->once())
            ->method('generate')
            ->will($this->returnValue('HASH'));

        $job
            ->expects($this->once())
            ->method('setCreatedAt')
            ->with($this->equalTo($dateTime));
        $job
            ->expects($this->once())
            ->method('setStatus')
            ->with($this->equalTo('new'));
        $job
            ->expects($this->once())
            ->method('setHash')
            ->with($this->equalTo('HASH'));

        $this->assertEquals($job, $this->_object->createJob($form));
    }

    public function dataProviderForTestCountByStatus() : array
    {
        return [
            [[], 0],
            [[1], 1],
            [[1, 2, 3, 4], 4],
        ];
    }

    public function testGetByHash() : void
    {
        $hash = 'DFGZU';

        $repository = $this
            ->getMockBuilder(\Doctrine\ORM\EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Job'))
            ->will($this->returnValue($repository));

        $repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with($this->equalTo(['hash' => $hash]))
            ->will($this->returnValue($job));

        $this->assertEquals($job, $this->_object->getByHash($hash));
    }

    public function testGetByStatus() : void
    {
        $repository = $this
            ->getMockBuilder(\Doctrine\ORM\EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $queryBuilder = $this
            ->getMockBuilder(\Doctrine\ORM\QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $expr = $this
            ->getMockBuilder(\Doctrine\DBAL\Query\Expression\ExpressionBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $query = $this
            ->getMockBuilder(\Doctrine\ORM\AbstractQuery::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Job'))
            ->will($this->returnValue($repository));

        $repository
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->with($this->equalTo('job'))
            ->will($this->returnValue($queryBuilder));

        $queryBuilder
            ->expects($this->at(0))
            ->method('expr')
            ->will($this->returnValue($expr));
        $queryBuilder
            ->expects($this->at(1))
            ->method('where')
            ->with($this->equalTo('in-expr'));
        $queryBuilder
            ->expects($this->at(2))
            ->method('setParameter')
            ->with(
                $this->equalTo('status'),
                $this->equalTo(['status', 'status2'])
            );
        $queryBuilder
            ->expects($this->at(3))
            ->method('orderBy')
            ->with(
                $this->equalTo('job.createdAt'),
                $this->equalTo('ASC')
            );
        $queryBuilder
            ->expects($this->at(4))
            ->method('getQuery')
            ->will($this->returnValue($query));

        $expr
            ->expects($this->once())
            ->method('in')
            ->with(
                $this->equalTo('job.status'),
                $this->equalTo(':status')
            )
            ->will($this->returnValue('in-expr'));

        $query
            ->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(['jobs']));

        $this->assertEquals(['jobs'], $this->_object->getByStatus(['status', 'status2']));
    }

    public function testGetNext() : void
    {
        $repository = $this
            ->getMockBuilder(\Doctrine\ORM\EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Job'))
            ->will($this->returnValue($repository));

        $repository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(
                $this->equalTo(['status' => 'new']),
                $this->equalTo(['createdAt' => 'asc'])
            )
            ->will($this->returnValue($job));

        $this->assertEquals($job, $this->_object->getNext());
    }

    public function testGetQueuePosition() : void
    {
        $queryBuilder = $this
            ->getMockBuilder(\Doctrine\ORM\QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $query = $this
            ->getMockBuilder(\Doctrine\ORM\AbstractQuery::class)
            ->disableOriginalConstructor()
            ->getMock();
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $datetime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $job
            ->expects($this->once())
            ->method('getCreatedAt')
            ->will($this->returnValue($datetime));

        $this->_entityManager
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder
            ->expects($this->at(0))
            ->method('select')
            ->with($this->equalTo('COUNT(job.id)'));
        $queryBuilder
            ->expects($this->at(1))
            ->method('from')
            ->with($this->equalTo('AppBundle:Job'), $this->equalTo('job'));
        $queryBuilder
            ->expects($this->at(2))
            ->method('where')
            ->with($this->equalTo('job.status = :status'));
        $queryBuilder
            ->expects($this->at(3))
            ->method('andWhere')
            ->with($this->equalTo('job.createdAt <= :time'));
        $queryBuilder
            ->expects($this->at(4))
            ->method('setParameter')
            ->with(
                $this->equalTo('status'),
                $this->equalTo('new')
            );
        $queryBuilder
            ->expects($this->at(5))
            ->method('setParameter')
            ->with(
                $this->equalTo('time'),
                $this->equalTo($datetime)
            );
        $queryBuilder
            ->expects($this->once())
            ->method('getQuery')
            ->will($this->returnValue($query));

        $query
            ->expects($this->once())
            ->method('getSingleScalarResult')
            ->will($this->returnValue("2"));

        $this->assertEquals(2, $this->_object->getQueuePosition($job));
    }

    public function testGetRemovables() : void
    {
        $datetime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();
        $repository = $this
            ->getMockBuilder(\Doctrine\ORM\EntityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $queryBuilder = $this
            ->getMockBuilder(\Doctrine\ORM\QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $expr = $this
            ->getMockBuilder(\Doctrine\DBAL\Query\Expression\ExpressionBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        $query = $this
            ->getMockBuilder(\Doctrine\ORM\AbstractQuery::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_time
            ->expects($this->once())
            ->method('getCurrentDateTime')
            ->will($this->returnValue($datetime));

        $datetime
            ->expects($this->once())
            ->method('modify')
            ->with($this->equalTo("-{$this->_deleteAfterDays} days"))
            ->will($this->returnSelf());

        $this->_entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo('AppBundle:Job'))
            ->will($this->returnValue($repository));

        $repository
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->with($this->equalTo('job'))
            ->will($this->returnValue($queryBuilder));

        $queryBuilder
            ->expects($this->at(0))
            ->method('expr')
            ->will($this->returnValue($expr));
        $queryBuilder
            ->expects($this->at(1))
            ->method('where')
            ->with($this->equalTo('expr-return'));
        $queryBuilder
            ->expects($this->at(2))
            ->method('setParameter')
            ->with(
                $this->equalTo('executedStatus'),
                $this->equalTo('executed')
            );
        $queryBuilder
            ->expects($this->at(3))
            ->method('setParameter')
            ->with(
                $this->equalTo('notExecutedStatus'),
                $this->equalTo(['failed', 'timeout'])
            );
        $queryBuilder
            ->expects($this->at(4))
            ->method('setParameter')
            ->with(
                $this->equalTo('time'),
                $this->equalTo($datetime)
            );
        $queryBuilder
            ->expects($this->at(5))
            ->method('getQuery')
            ->will($this->returnValue($query));

        $expr
            ->expects($this->at(0))
            ->method('eq')
            ->with($this->equalTo('job.status'), $this->equalTo(':executedStatus'))
            ->will($this->returnValue('[0,0]'));
        $expr
            ->expects($this->at(1))
            ->method('lte')
            ->with($this->equalTo('job.executedAt'), $this->equalTo(':time'))
            ->will($this->returnValue('[0,1]'));
        $expr
            ->expects($this->at(2))
            ->method('andX')
            ->with($this->equalTo('[0,0]'), $this->equalTo('[0,1]'))
            ->will($this->returnValue('[0]'));
        $expr
            ->expects($this->at(3))
            ->method('in')
            ->with($this->equalTo('job.status'), $this->equalTo(':notExecutedStatus'))
            ->will($this->returnValue('[1,0]'));
        $expr
            ->expects($this->at(4))
            ->method('lte')
            ->with($this->equalTo('job.startedAt'), $this->equalTo(':time'))
            ->will($this->returnValue('[1,1]'));
        $expr
            ->expects($this->at(5))
            ->method('andX')
            ->with($this->equalTo('[1,0]'), $this->equalTo('[1,1]'))
            ->will($this->returnValue('[1]'));
        $expr
            ->expects($this->at(6))
            ->method('orX')
            ->with($this->equalTo('[0]'), $this->equalTo('[1]'))
            ->will($this->returnValue('expr-return'));

        $query
            ->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(['jobs']));

        $this->assertEquals(['jobs'], $this->_object->getRemovables());
    }

    public function testSave() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->at(0))
            ->method('persist')
            ->with($this->equalTo($job));
        $this->_entityManager
            ->expects($this->at(1))
            ->method('flush');

        $this->_object->save($job);
    }

    public function testRemove() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_entityManager
            ->expects($this->at(0))
            ->method('remove')
            ->with($this->equalTo($job));
        $this->_entityManager
            ->expects($this->at(1))
            ->method('flush');

        $this->_object->remove($job);
    }
}
