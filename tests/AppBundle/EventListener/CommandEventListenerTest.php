<?php declare(strict_types = 1);

namespace Tests\AppBundle\EventListener;

use AppBundle\EventListener\CommandEventListener;

class CommandEventListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CommandEventListener
     */
    private $_object;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;

    public function setUp() : void
    {
        parent::setUp();

        $this->_logger = $this
            ->getMockBuilder(\Psr\Log\LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new CommandEventListener($this->_logger);
    }

    public function testOnConsoleException() : void
    {
        $event = $this
            ->getMockBuilder(\Symfony\Component\Console\Event\ConsoleExceptionEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command = $this
            ->getMockBuilder(\Symfony\Bundle\FrameworkBundle\Command\Command::class)
            ->setMethods(['getName'])
            ->disableOriginalConstructor()
            ->getMock();

        $exception = new ExceptionWrapper('exception-message');
        $exception->setFile('exception-file');
        $exception->setLine(15);

        $event
            ->expects($this->once())
            ->method('getCommand')
            ->will($this->returnValue($command));
        $event
            ->expects($this->once())
            ->method('getException')
            ->will($this->returnValue($exception));

        $command
            ->expects($this->once())
            ->method('getName')
            ->will($this->returnValue('command-name'));

        $this->_logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $this->equalTo(
                    'Tests\AppBundle\EventListener\ExceptionWrapper: exception-message (uncaught exception) at '
                    . 'exception-file line 15 while running console command `command-name`'
                ),
                $this->equalTo(['exception' => $exception])
            );

        $this->_object->onConsoleException($event);
    }

    public function testOnConsoleTerminate() : void
    {
        $event = $this
            ->getMockBuilder(\Symfony\Component\Console\Event\ConsoleTerminateEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command = $this
            ->getMockBuilder(\Symfony\Bundle\FrameworkBundle\Command\Command::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event
            ->expects($this->once())
            ->method('getExitCode')
            ->will($this->returnValue(0));
        $event
            ->expects($this->once())
            ->method('getCommand')
            ->will($this->returnValue($command));

        $this->_logger
            ->expects($this->never())
            ->method('warning');

        $this->_object->onConsoleTerminate($event);
    }

    public function testOnConsoleTerminateWhenFailed() : void
    {
        $event = $this
            ->getMockBuilder(\Symfony\Component\Console\Event\ConsoleTerminateEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command = $this
            ->getMockBuilder(\Symfony\Bundle\FrameworkBundle\Command\Command::class)
            ->setMethods(['getName'])
            ->disableOriginalConstructor()
            ->getMock();

        $event
            ->expects($this->once())
            ->method('getExitCode')
            ->will($this->returnValue(123));
        $event
            ->expects($this->once())
            ->method('getCommand')
            ->will($this->returnValue($command));

        $command
            ->expects($this->once())
            ->method('getName')
            ->will($this->returnValue('command-name'));

        $this->_logger
            ->expects($this->once())
            ->method('warning')
            ->with($this->equalTo('Command `command-name` exited with status code 123'));

        $this->_object->onConsoleTerminate($event);
    }

    public function testOnConsoleTerminateWhenFailedWithInvalidExitCode() : void
    {
        $event = $this
            ->getMockBuilder(\Symfony\Component\Console\Event\ConsoleTerminateEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command = $this
            ->getMockBuilder(\Symfony\Bundle\FrameworkBundle\Command\Command::class)
            ->setMethods(['getName'])
            ->disableOriginalConstructor()
            ->getMock();

        $event
            ->expects($this->once())
            ->method('getExitCode')
            ->will($this->returnValue(300));
        $event
            ->expects($this->once())
            ->method('getCommand')
            ->will($this->returnValue($command));
        $event
            ->expects($this->once())
            ->method('setExitCode')
            ->with($this->equalTo(255));

        $command
            ->expects($this->once())
            ->method('getName')
            ->will($this->returnValue('command-name'));

        $this->_logger
            ->expects($this->once())
            ->method('warning')
            ->with($this->equalTo('Command `command-name` exited with status code 255'));

        $this->_object->onConsoleTerminate($event);
    }
}

// @codingStandardsIgnoreStart
class ExceptionWrapper extends \Exception
{
    public function setFile(string $file) : void
    {
        $this->file = $file;
    }

    public function setLine(int $line) : void
    {
        $this->line = $line;
    }
}
// @codingStandardsIgnoreEnd
