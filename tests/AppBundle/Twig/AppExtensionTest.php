<?php declare(strict_types = 1);

namespace Tests\AppBundle\Twig;

use AppBundle\Twig\AppExtension;

class AppExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AppExtension
     */
    private $_object;

    /**
     * @var int
     */
    private $_jobTimeoutHours = 15;

    public function setUp() : void
    {
        parent::setUp();

        $this->_object = new AppExtension($this->_jobTimeoutHours);
    }

    public function testInstance() : void
    {
        $this->isInstanceOf(\Twig_Extension::class, $this->_object);
    }

    public function testGetFilters() : void
    {
        $filters = $this->_object->getFilters();

        $this->assertCount(2, $filters);

        $this->assertInstanceOf(\Twig_Filter::class, $filters[0]);
        $this->assertEquals('dateFormat', $filters[0]->getName());
        $this->assertEquals([$this->_object, 'dateFormatFilter'], $filters[0]->getCallable());

        $this->assertInstanceOf(\Twig_Filter::class, $filters[1]);
        $this->assertEquals('timeoutFormat', $filters[1]->getName());
        $this->assertEquals([$this->_object, 'timeoutFormatFilter'], $filters[1]->getCallable());
    }

    public function testDateFormatFilter() : void
    {
        $this->assertEquals(
            '2016-12-17 12:14:54',
            $this->_object->dateFormatFilter(
                new \DateTime('2016-12-17 12:14:54')
            )
        );
    }

    /**
     * @dataProvider dataProviderForTestTimeoutFormatFilter
     */
    public function testTimeoutFormatFilter($timeout, string $expected) : void
    {
        $this->assertEquals($expected, $this->_object->timeoutFormatFilter($timeout));
    }

    public function dataProviderForTestTimeoutFormatFilter()
    {
        return [
            [null, "{$this->_jobTimeoutHours} h"],
            [12, "12 h"],
        ];
    }
}
