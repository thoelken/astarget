<?php declare(strict_types = 1);

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Job;
use DateTime;
use ReflectionClass;

class JobTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Job
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new Job();
    }

    /**
     * @dataProvider dataProviderForTestGetterSetter
     */
    public function testGetterSetter(string $property, $value) : void
    {
        $setter = 'set' . ucfirst($property);
        $getter = 'get' . ucfirst($property);

        $this->_object->{$setter}($value);
        $this->assertEquals($value, $this->_object->{$getter}());
    }

    public function dataProviderForTestGetterSetter() : array
    {
        return [
            ['email', 'abc'],
            ['gcRatio', 45],
            ['createdAt', new DateTime()],
            ['status', 'REQUESTED'],
            ['executedAt', new DateTime()],
            ['length', 10],
            ['maxMononucStretches', 20],
            ['fastaLength', 10],
            ['startedAt', new DateTime()],
            ['timeout', 20],
            ['hash', 'ABC12345'],
        ];
    }

    public function testGetId() : void
    {
        $value = 15;

        $property = (new ReflectionClass($this->_object))->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($this->_object, $value);

        $this->assertEquals($value, $this->_object->getId());
    }
}
