<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\Authentication;

class AuthenticationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Symfony\Component\HttpFoundation\RequestStack
     */
    private $_request;

    /**
     * @var array
     */
    private $_validIps = ['127.0.0.1'];

    /**
     * @var array
     */
    private $_ipRanges = [['2.2.2.2','2.2.3.45']];

    public function setUp() : void
    {
        parent::setUp();

        $this->_request = $this
            ->getMockBuilder(\Symfony\Component\HttpFoundation\RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass(
            new Authentication($this->_request, '', $this->_validIps, $this->_ipRanges)
        );

        $this->assertEquals(
            [
                'ENV_NAME' => 'SWITCH_IP_FB',
            ],
            $object->getConstants()
        );
    }

    /**
     * @dataProvider dataProviderForTestHasAccessDevAndTest
     */
    public function testHasAccessDev($env, bool $expected) : void
    {
        $objectDev  = new Authentication($this->_request, 'dev', $this->_validIps, $this->_ipRanges);

        putenv("SWITCH_IP_FB={$env}");

        $this->assertEquals($expected, $objectDev->hasAccess());
    }

    /**
     * @dataProvider dataProviderForTestHasAccessDevAndTest
     */
    public function testHasAccessTest($env, bool $expected) : void
    {
        $objectTest = new Authentication($this->_request, 'test', $this->_validIps, $this->_ipRanges);

        putenv("SWITCH_IP_FB={$env}");

        $this->assertEquals($expected, $objectTest->hasAccess());
    }

    public function dataProviderForTestHasAccessDevAndTest() : array
    {
        return [
            [false, false],
            ['', false],
            ['2', false],
            ['false', false],
            ['true', true],
        ];
    }

    /**
     * @dataProvider dataProviderForTestHasAccessProd
     */
    public function testHasAccessProd(string $ipaddress, bool $expected) : void
    {
        $objectProd = new Authentication($this->_request, 'prod', $this->_validIps, $this->_ipRanges);

        $masterrequest = $this
            ->getMockBuilder(\Symfony\Component\HttpFoundation\Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_request
            ->expects($this->once())
            ->method('getMasterRequest')
            ->will($this->returnValue($masterrequest));

        $masterrequest
            ->expects($this->once())
            ->method('getClientIp')
            ->will($this->returnValue($ipaddress));

        $this->assertEquals($expected, $objectProd->hasAccess());
    }

    public function dataProviderForTestHasAccessProd() : array
    {
        return [
            ['127.0.0.1', true],
            ['1.5.15.122', false],
            ['2.2.2.2', true],
            ['2.2.3.45', true],
            ['2.2.2.1', false],
            ['2.2.3.46', false],
            ['2.2.2.255', true],
            ['2.2.2.6.9', false],
        ];
    }
}
