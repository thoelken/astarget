<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\Flash;

class FlashTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Flash
     */
    private $_object;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $_session;

    public function setUp() : void
    {
        parent::setUp();

        $this->_session = $this
            ->getMockBuilder(\Symfony\Component\HttpFoundation\Session\Session::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_object = new Flash($this->_session);
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'TYPE_ERROR'                  => 'error',
                'TYPE_SUCCESS'                => 'success',
                'MESSAGE_JOB_CREATED'         => 'Your request is currently queued/being processed.<br>'
                    . 'Depending on the queue, input length and search parameters, this can take several minutes/'
                    . 'hours. Please come back to this site later.',
                'MESSAGE_JOB_NOT_FOUND'       => 'Job not found.',
                'MESSAGE_JOB_REMOVED'         => 'Job successfully removed.',
                'MESSAGE_REMOVE_FAILED'       => 'Job could not be removed. Try again later or contact the admin.',
                'MESSAGE_REMOVE_WHILE_ACTIVE' => 'Job is currently executing. It can not be removed during execution.',
            ],
            $object->getConstants()
        );
    }

    public function testAddFlash() : void
    {
        $flashBag = $this
            ->getMockBuilder(\Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_session
            ->expects($this->once())
            ->method('getFlashBag')
            ->will($this->returnValue($flashBag));

        $flashBag
            ->expects($this->once())
            ->method('add')
            ->with(
                $this->equalTo('type'),
                $this->equalTo('message')
            );

        $this->_object->addFlash('type', 'message');
    }
}
