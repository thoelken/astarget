<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\FileReader;

class FileReaderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FileReader
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();

        $this->_object = new FileReader();
    }

    public function tearDown() : void
    {
        try {
            unlink("var/data/test.tmp");
        } catch (\Exception $ex) {
            // nothing to do
        }
        try {
            unlink("var/data/test_empty.tmp");
        } catch (\Exception $ex) {
            // nothing to do
        }

        parent::tearDown();
    }

    public function testReadFile() : void
    {
        $path = "var/data/test.tmp";
        $text = "some text to read";

        file_put_contents($path, $text);

        $this->assertEquals($text, $this->_object->readFile($path));
    }

    public function testReadFileException() : void
    {
        $path = "var/data/test_file_not_existing.tmp";

        $this->assertNull($this->_object->readFile($path));
    }

    public function testReadFileEmpty() : void
    {
        $path = "var/data/test_empty.tmp";

        file_put_contents($path, '');

        $this->assertNull($this->_object->readFile($path));
    }
}
