<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\MailMessage;

class MailMessageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MailMessage
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new MailMessage();
    }

    public function testGetMessage() : void
    {
        $this->assertInstanceOf(
            \Swift_Message::class,
            $this->_object->getMessage()
        );
    }
}
