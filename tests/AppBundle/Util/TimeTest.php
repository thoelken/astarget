<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\Time;

class TimeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Time
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new Time();
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'TIMEZONE' => 'Europe/Berlin',
            ],
            $object->getConstants()
        );
    }

    public function testGetCurrentDateTime() : void
    {
        $result = $this->_object->getCurrentDateTime();

        $this->assertInstanceOf(\DateTime::class, $result);
        $this->assertInstanceOf(\DateTimeZone::class, $result->getTimezone());
        $this->assertEquals('Europe/Berlin', $result->getTimezone()->getName());
    }
}
