<?php declare(strict_types = 1);

namespace Tests\AppBundle\Util;

use AppBundle\Util\ProcessBuilderFactory;

class ProcessBuilderFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ProcessBuilderFactory
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new ProcessBuilderFactory();
    }

    public function testGetProcessBuilder() : void
    {
        $this->assertInstanceOf(
            \Symfony\Component\Process\ProcessBuilder::class,
            $this->_object->getProcessBuilder()
        );
    }
}
