<?php declare(strict_types = 1);

namespace Tests\AppBundle\Command;

use AppBundle\Command\ExecuteJobCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\Container;

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 * @SuppressWarnings(PMD.ExcessiveMethodLength)
 */
class ExecuteJobCommandTest extends KernelTestCase
{
    /**
     * @var ExecuteJobCommand
     */
    private $_object;

    /**
     * @var CommandTester
     */
    private $_commandTester;

    public function setUp() : void
    {
        parent::setUp();

        $this->_container = new Container();

        $this->_container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'service.mail',
            $this
                ->getMockBuilder(\AppBundle\Service\Mail::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'service.process',
            $this
                ->getMockBuilder(\AppBundle\Service\Process::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'util.time',
            $this
                ->getMockBuilder(\AppBundle\Util\Time::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->set(
            'logger',
            $this
                ->getMockBuilder(\Symfony\Bridge\Monolog\Logger::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $this->_container->setParameter('app.max_concurrent_jobs', 2);
        $this->_container->setParameter('app.notify_job_execution_time', 100);

        $this->_object = new ExecuteJobCommand();

        self::bootKernel();

        $application = new Application(self::$kernel);
        $application->add($this->_object);

        $command = $application->find('app:execute-job');
        $command->setContainer($this->_container);

        $this->_commandTester = new CommandTester($command);
    }

    public function testInstance() : void
    {
        $this->assertInstanceOf(
            \AppBundle\Command\Command::class,
            $this->_object
        );
        $this->assertInstanceOf(
            \Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand::class,
            $this->_object
        );
    }

    public function testConstants() : void
    {
        $object = new \ReflectionClass($this->_object);

        $this->assertEquals(
            [
                'LOG_PREFIX' => 'ExecuteJobCommand',
            ],
            $object->getConstants()
        );
    }

    public function testExecute() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $process = $this
            ->getMockBuilder(\Symfony\Component\Process\Process::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dateTime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_container->get('service.job')
            ->expects($this->at(0))
            ->method('countByStatus')
            ->with($this->equalTo('active'))
            ->will($this->returnValue(1));
        $this->_container->get('service.job')
            ->expects($this->at(1))
            ->method('getNext')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(2))
            ->method('save')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(3))
            ->method('save')
            ->will($this->returnValue($job));

        $this->_container->get('service.process')
            ->expects($this->once())
            ->method('generatePrediction')
            ->with($this->equalTo($job))
            ->will($this->returnValue($process));

        $process
            ->expects($this->once())
            ->method('isSuccessful')
            ->will($this->returnValue(true));

        $this->_container->get('util.time')
            ->expects($this->exactly(2))
            ->method('getCurrentDateTime')
            ->will($this->returnValue($dateTime));

        $job
            ->expects($this->at(0))
            ->method('setStatus')
            ->with($this->equalTo('active'));
        $job
            ->expects($this->at(3))
            ->method('setStatus')
            ->with($this->equalTo('executed'));
        $job
            ->expects($this->once())
            ->method('setStartedAt')
            ->with($this->equalTo($dateTime));
        $job
            ->expects($this->once())
            ->method('setExecutedAt')
            ->with($this->equalTo($dateTime));
        $job
            ->expects($this->exactly(2))
            ->method('getId')
            ->will($this->returnValue(5));

        $this->_container->get('logger')
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo('ExecuteJobCommand: Starting job execution.'),
                $this->equalTo(['job-id' => 5])
            );
        $this->_container->get('logger')
            ->expects($this->at(1))
            ->method('info')
            ->with(
                $this->equalTo('ExecuteJobCommand: Successful job execution.'),
                $this->equalTo(['job-id' => 5])
            );
        $this->_container->get('logger')
            ->expects($this->never())
            ->method('error');

        $this->_container->get('service.mail')
            ->expects($this->once())
            ->method('sendFeedback')
            ->with($this->equalTo($job));

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }

    /**
     * @dataProvider dataProviderForTestExecuteWithMaxActiveJobs
     */
    public function testExecuteWithMaxActiveJobs(int $countJobs) : void
    {
        $this->_container->get('service.job')
            ->expects($this->once())
            ->method('countByStatus')
            ->with($this->equalTo('active'))
            ->will($this->returnValue($countJobs));

        $this->_container->get('logger')
            ->expects($this->once())
            ->method('info')
            ->with($this->equalTo("ExecuteJobCommand: No job started. {$countJobs} jobs already running."));

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }

    public function dataProviderForTestExecuteWithMaxActiveJobs() : array
    {
        return [
            [2],
            [3],
            [4],
        ];
    }

    public function testExecuteWithNoNewJobs() : void
    {
        $this->_container->get('service.job')
            ->expects($this->at(0))
            ->method('countByStatus')
            ->with($this->equalTo('active'))
            ->will($this->returnValue(1));
        $this->_container->get('service.job')
            ->expects($this->at(1))
            ->method('getNext')
            ->will($this->returnValue(null));

        $this->_container->get('logger')
            ->expects($this->once())
            ->method('info')
            ->with($this->equalTo('ExecuteJobCommand: No job started. No new jobs available.'));

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }

    public function testExecuteWhenProcessNotSuccessful() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $process = $this
            ->getMockBuilder(\Symfony\Component\Process\Process::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dateTime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_container->get('service.job')
            ->expects($this->at(0))
            ->method('countByStatus')
            ->with($this->equalTo('active'))
            ->will($this->returnValue(1));
        $this->_container->get('service.job')
            ->expects($this->at(1))
            ->method('getNext')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(2))
            ->method('save')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(3))
            ->method('save')
            ->will($this->returnValue($job));

        $this->_container->get('service.process')
            ->expects($this->once())
            ->method('generatePrediction')
            ->with($this->equalTo($job))
            ->will($this->returnValue($process));

        $process
            ->expects($this->once())
            ->method('isSuccessful')
            ->will($this->returnValue(false));
        $process
            ->expects($this->exactly(2))
            ->method('getErrorOutput')
            ->will($this->returnValue('stderr-output'));

        $this->_container->get('util.time')
            ->expects($this->once())
            ->method('getCurrentDateTime')
            ->will($this->returnValue($dateTime));

        $job
            ->expects($this->at(0))
            ->method('setStatus')
            ->with($this->equalTo('active'));
        $job
            ->expects($this->at(3))
            ->method('setStatus')
            ->with($this->equalTo('failed'));
        $job
            ->expects($this->once())
            ->method('setStartedAt')
            ->with($this->equalTo($dateTime));
        $job
            ->expects($this->never())
            ->method('setExecutedAt');
        $job
            ->expects($this->exactly(2))
            ->method('getId')
            ->will($this->returnValue(5));

        $this->_container->get('logger')
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo('ExecuteJobCommand: Starting job execution.'),
                $this->equalTo(['job-id' => 5])
            );
        $this->_container->get('logger')
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo('ExecuteJobCommand: Error in job execution.'),
                $this->equalTo(
                    [
                        'job-id' => 5,
                        'stderr' => 'stderr-output',
                    ]
                )
            );

        $this->_container->get('service.mail')
            ->expects($this->once())
            ->method('sendNotifyJobFailed')
            ->with($this->equalTo($job), $this->equalTo('stderr-output'));
        $this->_container->get('service.mail')
            ->expects($this->once())
            ->method('sendFeedback')
            ->with($this->equalTo($job));

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }

    public function testExecuteWhenProcessTimedOut() : void
    {
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dateTime = $this
            ->getMockBuilder(\DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->_container->get('service.job')
            ->expects($this->at(0))
            ->method('countByStatus')
            ->with($this->equalTo('active'))
            ->will($this->returnValue(1));
        $this->_container->get('service.job')
            ->expects($this->at(1))
            ->method('getNext')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(2))
            ->method('save')
            ->will($this->returnValue($job));
        $this->_container->get('service.job')
            ->expects($this->at(3))
            ->method('save')
            ->will($this->returnValue($job));

        $this->_container->get('service.process')
            ->expects($this->once())
            ->method('generatePrediction')
            ->with($this->equalTo($job))
            ->will($this->returnValue(null));

        $this->_container->get('util.time')
            ->expects($this->once())
            ->method('getCurrentDateTime')
            ->will($this->returnValue($dateTime));

        $job
            ->expects($this->at(0))
            ->method('setStatus')
            ->with($this->equalTo('active'));
        $job
            ->expects($this->at(3))
            ->method('setStatus')
            ->with($this->equalTo('timeout'));
        $job
            ->expects($this->once())
            ->method('setStartedAt')
            ->with($this->equalTo($dateTime));
        $job
            ->expects($this->never())
            ->method('setExecutedAt');
        $job
            ->expects($this->exactly(2))
            ->method('getId')
            ->will($this->returnValue(5));

        $this->_container->get('logger')
            ->expects($this->at(0))
            ->method('info')
            ->with(
                $this->equalTo('ExecuteJobCommand: Starting job execution.'),
                $this->equalTo(['job-id' => 5])
            );
        $this->_container->get('logger')
            ->expects($this->at(1))
            ->method('error')
            ->with(
                $this->equalTo('ExecuteJobCommand: Job execution timed out.'),
                $this->equalTo(['job-id' => 5])
            );

        $this->_container->get('service.mail')
            ->expects($this->once())
            ->method('sendNotifyJobTimeout')
            ->with($this->equalTo($job));
        $this->_container->get('service.mail')
            ->expects($this->once())
            ->method('sendFeedback')
            ->with($this->equalTo($job));

        $this->assertEquals(0, $this->_commandTester->execute(['command' => $this->_object->getName()]));
    }
}
