<?php declare(strict_types = 1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testIndex() : void
    {
        $client = static::createClient();

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }
}
