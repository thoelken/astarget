<?php declare(strict_types = 1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 */
class ResultControllerTest extends WebTestCase
{
    public function testIndexAction() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $job = new \AppBundle\Entity\Job();
        $job->setExecutedAt(new \DateTime());
        $job->setStartedAt(new \DateTime());
        $job->setGcRatio(30);
        $job->setLength(15);
        $job->setFastaLength(7245);
        $job->setMaxMononucStretches(5);
        $job->setHash('ABC');
        $job->setStatus('executed');

        $property = (new \ReflectionClass($job))->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($job, 1);

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.authentication',
            $this
                ->getMockBuilder(\AppBundle\Util\Authentication::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));
        $container->get('service.job')
            ->expects($this->once())
            ->method('getQueuePosition')
            ->with($this->equalTo($job))
            ->will($this->returnValue(4));

        $container->get('util.authentication')
            ->expects($this->once())
            ->method('hasAccess')
            ->will($this->returnValue(true));

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testIndexActionWithRemovedJob() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $job = new \AppBundle\Entity\Job();
        $job->setStatus('removed');

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));

        $crawler = $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode()
        );

        $this->assertContains('Job not found.', $crawler->text());
    }

    public function testIndexActionWithNotFound() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue(null));

        $crawler = $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode()
        );

        $this->assertContains('Job not found.', $crawler->text());
    }

    public function testDownloadAction() : void
    {
        $path = __DIR__ . '/../Resources/test-download.txt';

        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.path',
            $this
                ->getMockBuilder(\AppBundle\Util\Path::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'filesystem',
            $this
                ->getMockBuilder(\Symfony\Component\Filesystem\Filesystem::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));

        $container->get('util.path')
            ->expects($this->once())
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('name'))
            ->will($this->returnValue($path));

        $container->get('filesystem')
            ->expects($this->once())
            ->method('exists')
            ->with($this->equalTo($path))
            ->will($this->returnValue(true));

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/download/name'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\BinaryFileResponse::class, $client->getResponse());
        $this->assertEquals($path, $client->getResponse()->getFile()->getPathname());
        $this->assertEquals(
            'attachment; filename="test-download.txt"',
            $client->getResponse()->headers->get('Content-Disposition')
        );
    }

    public function testDownloadActionWhenFileNotExists() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.path',
            $this
                ->getMockBuilder(\AppBundle\Util\Path::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'filesystem',
            $this
                ->getMockBuilder(\Symfony\Component\Filesystem\Filesystem::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $job = $this
            ->getMockBuilder(\AppBundle\Entity\Job::class)
            ->disableOriginalConstructor()
            ->getMock();

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));

        $container->get('util.path')
            ->expects($this->once())
            ->method('getFilePath')
            ->with($this->equalTo($job), $this->equalTo('name'))
            ->will($this->returnValue('path/to/file'));

        $container->get('filesystem')
            ->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('path/to/file'))
            ->will($this->returnValue(false));

        $crawler = $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/download/name'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode()
        );

        $this->assertContains('File not found.', $crawler->text());
    }

    public function testDownloadActionWhenJobIsNull() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue(null));

        $crawler = $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/download/name'
        );

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
            $client->getResponse()->getStatusCode()
        );

        $this->assertContains('File not found.', $crawler->text());
    }

    /**
     * @dataProvider dataProviderForTestRemoveAction
     */
    public function testRemoveAction($job, string $flashMessage) : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.flash',
            $this
                ->getMockBuilder(\AppBundle\Util\Flash::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));

        $container->get('util.flash')
            ->expects($this->once())
            ->method('addFlash')
            ->with($this->equalTo(\AppBundle\Util\Flash::TYPE_ERROR), $this->equalTo($flashMessage));

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/remove'
        );
        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_FOUND,
            $client->getResponse()->getStatusCode()
        );
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $client->getResponse());

        $client->followRedirect();

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    public function dataProviderForTestRemoveAction() : array
    {
        $removedJob = new \AppBundle\Entity\Job();
        $removedJob->setStatus(\AppBundle\Service\Job::STATUS_REMOVED);

        $activeJob = new \AppBundle\Entity\Job();
        $activeJob->setStatus(\AppBundle\Service\Job::STATUS_ACTIVE);

        return [
            [null, \AppBundle\Util\Flash::MESSAGE_JOB_NOT_FOUND],
            [$removedJob, \AppBundle\Util\Flash::MESSAGE_JOB_NOT_FOUND],
            [$activeJob, \AppBundle\Util\Flash::MESSAGE_REMOVE_WHILE_ACTIVE],
        ];
    }

    public function testRemoveActionWhenDirectoryRemoveFailed() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $job = new \AppBundle\Entity\Job();
        $job->setStatus(\AppBundle\Service\Job::STATUS_NEW);

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'service.filesystem',
            $this
                ->getMockBuilder(\AppBundle\Service\Filesystem::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.flash',
            $this
                ->getMockBuilder(\AppBundle\Util\Flash::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));

        $container->get('service.filesystem')
            ->expects($this->once())
            ->method('removeJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue(false));

        $container->get('util.flash')
            ->expects($this->once())
            ->method('addFlash')
            ->with(
                $this->equalTo(\AppBundle\Util\Flash::TYPE_ERROR),
                $this->equalTo(\AppBundle\Util\Flash::MESSAGE_REMOVE_FAILED)
            );

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/remove'
        );
        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_FOUND,
            $client->getResponse()->getStatusCode()
        );
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $client->getResponse());

        $client->followRedirect();

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testRemoveActionWhenDirectoryRemoveSuccessful() : void
    {
        $client    = static::createClient();
        $container = $client->getContainer();

        $job = new \AppBundle\Entity\Job();
        $job->setStatus(\AppBundle\Service\Job::STATUS_NEW);

        $container->set(
            'service.job',
            $this
                ->getMockBuilder(\AppBundle\Service\Job::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'service.filesystem',
            $this
                ->getMockBuilder(\AppBundle\Service\Filesystem::class)
                ->disableOriginalConstructor()
                ->getMock()
        );
        $container->set(
            'util.flash',
            $this
                ->getMockBuilder(\AppBundle\Util\Flash::class)
                ->disableOriginalConstructor()
                ->getMock()
        );

        $container->get('service.job')
            ->expects($this->once())
            ->method('getByHash')
            ->with($this->equalTo('ABC'))
            ->will($this->returnValue($job));
        $container->get('service.job')
            ->expects($this->once())
            ->method('save')
            ->with($this->equalTo($job));

        $container->get('service.filesystem')
            ->expects($this->once())
            ->method('removeJobDirectory')
            ->with($this->equalTo($job))
            ->will($this->returnValue(true));

        $container->get('util.flash')
            ->expects($this->once())
            ->method('addFlash')
            ->with(
                $this->equalTo(\AppBundle\Util\Flash::TYPE_SUCCESS),
                $this->equalTo(\AppBundle\Util\Flash::MESSAGE_JOB_REMOVED)
            );

        $client->request(
            \Symfony\Component\HttpFoundation\Request::METHOD_GET,
            '/result/ABC/remove'
        );
        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_FOUND,
            $client->getResponse()->getStatusCode()
        );
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $client->getResponse());

        $client->followRedirect();

        $this->assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->getResponse()->getStatusCode()
        );

        $this->assertEquals(\AppBundle\Service\Job::STATUS_REMOVED, $job->getStatus());
    }
}
