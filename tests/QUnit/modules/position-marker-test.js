(function ($) {
    "use strict";

    var markerSelector = "#qunit-fixture .result--position-pointer-box span.result--position-pointer-marker";

    QUnit.module("position-marker", {
        beforeEach: function () {
            // Reset fastaLength and subSequenceLength
            $.test.publish("view.fastaLength", undefined);
            $.test.publish("view.subSequenceLength", undefined);
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual(
            $.subscriptions,
            ["position-marker.init", "view.fastaLength", "view.subSequenceLength"]
        );
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("mouseenter", function (assert) {
        $.test.publish("view.fastaLength", 1000);
        $.test.publish("view.subSequenceLength", 20);

        $("#test-td").text("500");

        $.test.publish("position-marker.init");

        assert.strictEqual($(markerSelector).length, 0);

        $("#test-tr").trigger("mouseenter");

        assert.strictEqual($(markerSelector).length, 1);
        assert.strictEqual($(markerSelector).attr("style"), "background-color: rgb(240, 40, 40); left: 50%; width: 2%;");

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("mouseenter multiple values", function (assert) {
        $.test.publish("view.fastaLength", 800);
        $.test.publish("view.subSequenceLength", 30);

        $("#test-td").text("500,612,713");

        $.test.publish("position-marker.init");

        assert.strictEqual($(markerSelector).length, 0);

        $("#test-tr").trigger("mouseenter");

        assert.strictEqual($(markerSelector).length, 3);
        assert.strictEqual($(markerSelector).eq(0).attr("style"), "background-color: rgba(240, 40, 40, 0.701961); left: 89.125%; width: 3.75%;");
        assert.strictEqual($(markerSelector).eq(1).attr("style"), "background-color: rgba(240, 40, 40, 0.701961); left: 76.5%; width: 3.75%;");
        assert.strictEqual($(markerSelector).eq(2).attr("style"), "background-color: rgba(240, 40, 40, 0.701961); left: 62.5%; width: 3.75%;");

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("set without view events", function (assert) {
        $("#test-td").text("23");

        $.test.publish("position-marker.init");

        assert.strictEqual($(markerSelector).length, 0);

        $("#test-tr").trigger("mouseenter");

        assert.strictEqual($(markerSelector).length, 0);

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("mouseleave", function (assert) {
        $.test.publish("view.fastaLength", 800);
        $.test.publish("view.subSequenceLength", 30);

        $("#test-td").text("12,24");

        $.test.publish("position-marker.init");

        assert.strictEqual($(markerSelector).length, 0);

        $("#test-tr").trigger("mouseenter");

        assert.strictEqual($(markerSelector).length, 2);

        $("#test-tr").trigger("mouseleave");

        assert.strictEqual($(markerSelector).length, 0);

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
