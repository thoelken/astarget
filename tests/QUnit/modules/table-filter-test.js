(function ($) {
    "use strict";

    var inputs = "#qunit-fixture .result--table input";

    QUnit.module("table-filter", {
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["table-filter.init"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("filter", function (assert) {
        $.test.publish("table-filter.init");

        $(inputs).eq(0).val(10);
        $(inputs).eq(3).val(66);

        $(inputs).eq(0).trigger($.Event("keypress", { which : 13 }));

        assert.deepEqual(
            $.publishings,
            [
                {
                    name : "result-table.filter",
                    data : [
                        [
                            {
                                index : 0,
                                type  : "min",
                                value : "10",
                            },
                            {
                                index : 1,
                                type  : "max",
                                value : "66",
                            }
                        ]
                    ]
                }
            ]
        );
    });

    QUnit.test("filter reset", function (assert) {
        $.test.publish("table-filter.init");

        $(inputs).eq(0).trigger($.Event("keypress", { which : 13 }));

        assert.deepEqual(
            $.publishings,
            [
                {
                    name : "result-table.filter",
                    data : [[]]
                }
            ]
        );
    });

    QUnit.test("filter with various keypress events", function (assert) {
        $.test.publish("table-filter.init");

        for (var i = 0; i < 100; i++) {
            if (i === 13) {
                continue;
            }
            $(inputs).eq(0).trigger($.Event("keypress", { which : i }));
        }

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
