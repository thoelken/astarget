(function ($) {
    "use strict";

    QUnit.module("overlay", {
        beforeEach: function () {
            $.test.publish("overlay.resetCounter");
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["overlay.hide", "overlay.resetCounter", "overlay.show"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("show", function (assert) {
        $.test.publish("overlay.show");
        assert.ok($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("show and hide", function (assert) {
        $.test.publish("overlay.show");
        assert.ok($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        $.test.publish("overlay.hide");
        assert.notOk($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("multiple shows and hides", function (assert) {
        $.test.publish("overlay.show");
        assert.ok($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        $.test.publish("overlay.show");
        assert.ok($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        $.test.publish("overlay.hide");
        assert.ok($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        $.test.publish("overlay.hide");
        assert.notOk($("#qunit-fixture .result--overlay").hasClass("result--overlay__visible"));

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
