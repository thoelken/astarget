(function ($) {
    "use strict";

    QUnit.module("result-table", {
        beforeEach: function () {
            $.test.publish("result-table.init", [undefined]);
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual(
            $.subscriptions,
            ["result-table.filter", "result-table.init", "result-table.sort"]
        );
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init", function (assert) {
        var done = assert.async();

        var tableData = [
            [
                { rawValue: "abc" },
                { rawValue: "def" },
            ],
            [
                { rawValue: "ghi" },
                { rawValue: "jkl" },
            ],
        ];

        $.test.publish("result-table.init", [tableData]);

        setTimeout(function () {
            var trs = $("#qunit-fixture .result--table tbody tr");

            assert.strictEqual(trs.eq(0).find("td").eq(0).text(), "abc");
            assert.strictEqual(trs.eq(0).find("td").eq(1).text(), "def");
            assert.strictEqual(trs.eq(1).find("td").eq(0).text(), "ghi");
            assert.strictEqual(trs.eq(1).find("td").eq(1).text(), "jkl");

            assert.deepEqual($.publishings, ["overlay.show", "position-marker.init", "overlay.hide"]);

            done();
        }, 10);
    });

    QUnit.test("init with empty table", function (assert) {
        var done = assert.async();

        $.test.publish("result-table.init", [[]]);

        setTimeout(function () {
            assert.strictEqual($("#qunit-fixture .result--table tbody tr").length, 0);

            assert.deepEqual($.publishings, ["overlay.show", "position-marker.init", "overlay.hide"]);

            done();
        }, 10);
    });

    QUnit.test("init and sort", function (assert) {
        var done = assert.async();

        var tableData = [
            [
                { rawValue: "a", value: 1 },
            ],
            [
                { rawValue: "b", value: 3 },
            ],
            [
                { rawValue: "c", value: 2 },
            ],
        ];

        $.test.publish("result-table.init", [tableData]);

        $.test.publish("result-table.sort", [0, 1]);

        setTimeout(function () {
            var trs = $("#qunit-fixture .result--table tbody tr");

            assert.strictEqual(trs.eq(0).find("td").eq(0).text(), "a");
            assert.strictEqual(trs.eq(1).find("td").eq(0).text(), "c");
            assert.strictEqual(trs.eq(2).find("td").eq(0).text(), "b");

            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    "overlay.show",
                    "position-marker.init",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                ]
            );

            done();
        }, 100);
    });

    QUnit.test("sort without table data", function (assert) {
        $.test.publish("result-table.sort", [1, 1]);

        assert.notOk($("#qunit-fixture .result--table tbody tr").length);

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init and filter", function (assert) {
        var done = assert.async();

        var tableData = [
            [
                { rawValue: "1", value: 1 },
                { rawValue: "5", value: 5 },
            ],
            [
                { rawValue: "2", value: 2 },
                { rawValue: "8", value: 8 },
            ],
            [
                { rawValue: "3", value: 3 },
                { rawValue: "1", value: 1 },
            ],
        ];

        var filterData = [
            {
                index : 0,
                type  : "min",
                value : "2",
            },
            {
                index : 1,
                type  : "max",
                value : "1",
            }
        ];

        $.test.publish("result-table.init", [tableData]);

        $.test.publish("result-table.filter", [filterData]);

        setTimeout(function () {
            var trs = $("#qunit-fixture .result--table tbody tr");

            assert.strictEqual(trs.length, 1);

            assert.strictEqual(trs.eq(0).find("td").eq(0).text(), "3");
            assert.strictEqual(trs.eq(0).find("td").eq(1).text(), "1");

            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    "overlay.show",
                    "position-marker.init",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                ]
            );

            done();
        }, 100);
    });

    QUnit.test("filter without table data", function (assert) {
        $.test.publish("result-table.filter", [0, 1]);

        assert.notOk($("#qunit-fixture .result--table tbody tr").length);

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init, sort and filter", function (assert) {
        var done = assert.async();

        var tableData = [
            [
                { rawValue: "1", value: 1 },
                { rawValue: "5", value: 5 },
            ],
            [
                { rawValue: "2", value: 2 },
                { rawValue: "8", value: 8 },
            ],
            [
                { rawValue: "3", value: 3 },
                { rawValue: "1", value: 1 },
            ],
            [
                { rawValue: "4", value: 4 },
                { rawValue: "7", value: 7 },
            ],
        ];

        var filterData = [
            {
                index : 0,
                type  : "max",
                value : "3",
            }
        ];

        $.test.publish("result-table.init", [tableData]);

        $.test.publish("result-table.sort", [1, -1]);

        $.test.publish("result-table.filter", [filterData]);

        setTimeout(function () {
            var trs = $("#qunit-fixture .result--table tbody tr");

            assert.strictEqual(trs.length, 3);

            assert.strictEqual(trs.eq(0).find("td").eq(0).text(), "2");
            assert.strictEqual(trs.eq(0).find("td").eq(1).text(), "8");
            assert.strictEqual(trs.eq(1).find("td").eq(0).text(), "1");
            assert.strictEqual(trs.eq(1).find("td").eq(1).text(), "5");
            assert.strictEqual(trs.eq(2).find("td").eq(0).text(), "3");
            assert.strictEqual(trs.eq(2).find("td").eq(1).text(), "1");

            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    "overlay.show",
                    "overlay.show",
                    "position-marker.init",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                ]
            );

            done();
        }, 100);
    });

    QUnit.test("init, filter and sort", function (assert) {
        var done = assert.async();

        var tableData = [
            [
                { rawValue: "1", value: 1 },
                { rawValue: "5", value: 5 },
            ],
            [
                { rawValue: "2", value: 2 },
                { rawValue: "8", value: 8 },
            ],
            [
                { rawValue: "3", value: 3 },
                { rawValue: "1", value: 1 },
            ],
            [
                { rawValue: "4", value: 4 },
                { rawValue: "7", value: 7 },
            ],
        ];

        var filterData = [
            {
                index : 0,
                type  : "max",
                value : "3",
            }
        ];

        $.test.publish("result-table.init", [tableData]);

        $.test.publish("result-table.filter", [filterData]);

        $.test.publish("result-table.sort", [1, 1]);

        setTimeout(function () {
            var trs = $("#qunit-fixture .result--table tbody tr");

            assert.strictEqual(trs.length, 3);

            assert.strictEqual(trs.eq(0).find("td").eq(0).text(), "3");
            assert.strictEqual(trs.eq(0).find("td").eq(1).text(), "1");
            assert.strictEqual(trs.eq(1).find("td").eq(0).text(), "1");
            assert.strictEqual(trs.eq(1).find("td").eq(1).text(), "5");
            assert.strictEqual(trs.eq(2).find("td").eq(0).text(), "2");
            assert.strictEqual(trs.eq(2).find("td").eq(1).text(), "8");

            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    "overlay.show",
                    "overlay.show",
                    "position-marker.init",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "overlay.show",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                    "position-marker.init",
                    "overlay.hide",
                ]
            );

            done();
        }, 100);
    });

}(jQuery));
