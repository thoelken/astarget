(function ($) {
    "use strict";

    QUnit.module("index-form", {
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["index-form.init"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init", function (assert) {
        $.test.publish("index-form.init");

        assert.ok($("#qunit-fixture .fastatext").hasClass("hidden"));
        assert.notOk($("#qunit-fixture .fastafile").hasClass("hidden"));

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init with change to 0", function (assert) {
        $.test.publish("index-form.init");

        $("#index_uploadfilechoice").val(0).change();

        assert.notOk($("#qunit-fixture .fastatext").hasClass("hidden"));
        assert.ok($("#qunit-fixture .fastafile").hasClass("hidden"));

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init with change to 1", function (assert) {
        $.test.publish("index-form.init");

        $("#index_uploadfilechoice").val(1).change();

        assert.ok($("#qunit-fixture .fastatext").hasClass("hidden"));
        assert.notOk($("#qunit-fixture .fastafile").hasClass("hidden"));

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
