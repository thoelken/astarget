(function ($) {
    "use strict";

    QUnit.module("fasta-load", {
        beforeEach: function () {
            $.test.publish("view.fastaInputUrl", undefined);
            $("#qunit-fixture .result--fasta").text("");
        },
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["fasta-load.load", "view.fastaInputUrl"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("load", function (assert) {
        var done = assert.async();

        $.mockjax({
            url: "/path/to/fasta",
            responseText: "FASTA string bla blub"
        });

        $.test.publish("view.fastaInputUrl", "/path/to/fasta");

        $.test.publish("fasta-load.load");

        assert.strictEqual($.mockjax.mockedAjaxCalls().length, 1);
        assert.strictEqual($.mockjax.unmockedAjaxCalls().length, 0);

        setTimeout(function () {
            assert.strictEqual($("#qunit-fixture .result--fasta").text(), "FASTA string bla blub");
            assert.deepEqual(
                $.publishings,
                [
                    "overlay.show",
                    "overlay.hide",
                ]
            );
            done();
        }, 100);
    });

    QUnit.test("load without view events", function (assert) {
        $.test.publish("fasta-load.load");

        assert.strictEqual($("#qunit-fixture .result--fasta").text(), "");

        assert.strictEqual($.mockjax.mockedAjaxCalls().length, 0);
        assert.strictEqual($.mockjax.unmockedAjaxCalls().length, 0);

        assert.deepEqual($.publishings, []);
    });

}(jQuery));
