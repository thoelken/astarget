(function ($) {
    "use strict";

    QUnit.module("link-confirm", {
        afterEach: function () {
            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["link-confirm.init"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("init, click on link and cancel", function (assert) {
        var done = assert.async();

        $.test.publish("link-confirm.init");

        assert.notOk($("#qunit-fixture .confirm").is(":visible"));

        $("#test-anchor").trigger("click");

        assert.ok($("#qunit-fixture .confirm").is(":visible"));
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").text(),
            "action text"
        );
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").attr("href"),
            "test/uri"
        );

        $("#qunit-fixture .confirm .confirm--cancel").trigger("click");

        setTimeout(function () {
            assert.notOk($("#qunit-fixture .confirm").is(":visible"));
            assert.deepEqual($.publishings, []);
            done();
        }, 250);
    });

    QUnit.test("init, click on link and click on confirm box", function (assert) {
        var done = assert.async();

        $.test.publish("link-confirm.init");

        assert.notOk($("#qunit-fixture .confirm").is(":visible"));

        $("#test-anchor").trigger("click");

        assert.ok($("#qunit-fixture .confirm").is(":visible"));
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").text(),
            "action text"
        );
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").attr("href"),
            "test/uri"
        );

        $("#qunit-fixture .confirm").trigger("click");

        setTimeout(function () {
            assert.ok($("#qunit-fixture .confirm").is(":visible"));
            assert.deepEqual($.publishings, []);
            done();
        }, 250);
    });

    QUnit.test("init, click on link and click on document", function (assert) {
        var done = assert.async();

        $.test.publish("link-confirm.init");

        assert.notOk($("#qunit-fixture .confirm").is(":visible"));

        $("#test-anchor").trigger("click");

        assert.ok($("#qunit-fixture .confirm").is(":visible"));
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").text(),
            "action text"
        );
        assert.strictEqual(
            $("#qunit-fixture .confirm .confirm--action").attr("href"),
            "test/uri"
        );

        $(document).trigger("click");

        setTimeout(function () {
            assert.notOk($("#qunit-fixture .confirm").is(":visible"));
            assert.deepEqual($.publishings, []);
            done();
        }, 250);
    });

}(jQuery));
