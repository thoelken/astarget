/**
 * Provides basic settings and functions for testing.
 * Decorates function of pubsub for better testability.
 */
(function ($) {
    "use strict";

    var subscribe = $.subscribe;
    var publish   = $.publish;

    $.subscriptions = [];
    $.publishings   = [];

    $.mockjaxSettings.responseTime = 0;

    $.test = {
        afterEach: function () {
            $.mockjax.clear();

            $.publishings = [];
        },
        subscribe: subscribe,
        publish: publish,
    };

    $.subscribe = function (name, callback) {
        $.subscriptions.push(name);
        subscribe(name, callback);
    };

    $.publish = function (name, data) {
        if (data === undefined) {
            $.publishings.push(name);
        } else {
            $.publishings.push({ name : name, data : data });
        }

        publish(name, data);
    };

}(jQuery));
