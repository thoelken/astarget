(function ($) {
    "use strict";

    QUnit.module("main", {
        afterEach: function () {
            $("body").attr("class", "");

            $.test.afterEach();
        }
    });

    QUnit.test("activate", function (assert) {
        assert.deepEqual($.subscriptions, ["document.ready"]);
        assert.deepEqual($.publishings, []);
    });

    QUnit.test("default", function (assert) {
        $.test.publish("document.ready");

        assert.deepEqual($.publishings, []);
    });

    QUnit.test("index", function (assert) {
        $("body").addClass("index");

        $.test.publish("document.ready");

        assert.deepEqual(
            $.publishings,
            ["index-form.init"]
        );
    });

    QUnit.test("result__new", function (assert) {
        $("body").addClass("result__new");

        $.test.publish("document.ready");

        assert.deepEqual(
            $.publishings,
            [
                "fasta-load.load",
                "link-confirm.init",
                { name: "refresh-page.timeout", data: 10 * 60 },
                "refresh-page.init",
            ]
        );
    });

    QUnit.test("result__info", function (assert) {
        $("body").addClass("result__info");

        $.test.publish("document.ready");

        assert.deepEqual(
            $.publishings,
            ["fasta-load.load", "link-confirm.init"]
        );
    });

    QUnit.test("result", function (assert) {
        $("body").addClass("result");

        $.test.publish("document.ready");

        assert.deepEqual(
            $.publishings,
            ["fasta-load.load", "table-load.load", "table-sort.init", "table-filter.init", "link-confirm.init"]
        );
    });

    QUnit.test("admin", function (assert) {
        $("body").addClass("admin");

        $.test.publish("document.ready");

        assert.deepEqual(
            $.publishings,
            ["link-confirm.init"]
        );
    });

}(jQuery));
