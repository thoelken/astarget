<?php declare(strict_types = 1);

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 */
class AppKernelTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AppKernel
     */
    private $_object;

    public function setUp() : void
    {
        parent::setUp();
        $this->_object = new AppKernel('environment', false);
    }

    public function testInstance() : void
    {
        $this->assertInstanceOf(Symfony\Component\HttpKernel\Kernel::class, $this->_object);
    }

    /**
     * @dataProvider dataProviderForTestRegisterBundles
     */
    public function testRegisterBundles(string $environment, array $expected) : void
    {
        $object = new AppKernel($environment, false);

        $bundles = $object->registerBundles();

        $classNamesOfBundles = array_map(
            function ($value) : string {
                return get_class($value);
            },
            $bundles
        );

        sort($classNamesOfBundles);
        sort($expected);

        $this->assertEquals($expected, $classNamesOfBundles);
    }

    public function dataProviderForTestRegisterBundles() : array
    {
        return [
            [
                'dev',
                [
                    \AppBundle\AppBundle::class,
                    \Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class,
                    \Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class,
                    \Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class,
                    \Sensio\Bundle\DistributionBundle\SensioDistributionBundle::class,
                    \Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class,
                    \Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle::class,
                    \Symfony\Bundle\AsseticBundle\AsseticBundle::class,
                    \Symfony\Bundle\DebugBundle\DebugBundle::class,
                    \Symfony\Bundle\FrameworkBundle\FrameworkBundle::class,
                    \Symfony\Bundle\MonologBundle\MonologBundle::class,
                    \Symfony\Bundle\SecurityBundle\SecurityBundle::class,
                    \Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class,
                    \Symfony\Bundle\TwigBundle\TwigBundle::class,
                    \Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class,
                ],
            ],
            [
                'test',
                [
                    \AppBundle\AppBundle::class,
                    \Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class,
                    \Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class,
                    \Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class,
                    \Sensio\Bundle\DistributionBundle\SensioDistributionBundle::class,
                    \Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class,
                    \Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle::class,
                    \Symfony\Bundle\AsseticBundle\AsseticBundle::class,
                    \Symfony\Bundle\DebugBundle\DebugBundle::class,
                    \Symfony\Bundle\FrameworkBundle\FrameworkBundle::class,
                    \Symfony\Bundle\MonologBundle\MonologBundle::class,
                    \Symfony\Bundle\SecurityBundle\SecurityBundle::class,
                    \Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class,
                    \Symfony\Bundle\TwigBundle\TwigBundle::class,
                    \Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class,
                ],
            ],
            [
                'prod',
                [
                    \AppBundle\AppBundle::class,
                    \Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class,
                    \Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class,
                    \Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class,
                    \Symfony\Bundle\AsseticBundle\AsseticBundle::class,
                    \Symfony\Bundle\FrameworkBundle\FrameworkBundle::class,
                    \Symfony\Bundle\MonologBundle\MonologBundle::class,
                    \Symfony\Bundle\SecurityBundle\SecurityBundle::class,
                    \Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class,
                    \Symfony\Bundle\TwigBundle\TwigBundle::class,
                ],
            ],
        ];
    }

    public function testGetRootDir() : void
    {
        $this->assertEquals(dirname(__DIR__) . '/app', $this->_object->getRootDir());
    }

    /**
     * @dataProvider dataProviderForTestGetCacheDir
     */
    public function testGetCacheDir(string $environment) : void
    {
        $object = new AppKernel($environment, false);

        $this->assertEquals(dirname(__DIR__) . '/var/cache/' . $environment, $object->getCacheDir());
    }

    public function dataProviderForTestGetCacheDir() : array
    {
        return [
            ['prod'],
            ['test'],
            ['dev'],
            ['hjdkslhfj'],
        ];
    }

    public function testGetLogDir() : void
    {
        $this->assertEquals(dirname(__DIR__) . '/var/logs', $this->_object->getLogDir());
    }

    /**
     * @dataProvider dataProviderForTestRegisterContainerConfiguration
     */
    public function testRegisterContainerConfiguration(string $environment) : void
    {
        $object = new AppKernel($environment, false);

        $loader = $this
            ->getMockBuilder(Symfony\Component\Config\Loader\LoaderInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $loader
            ->expects($this->once())
            ->method('load')
            ->with(
                $this->equalTo(
                    dirname(__DIR__) . '/app/config/config_' . $environment . '.yml'
                )
            );

        $object->registerContainerConfiguration($loader);
    }

    public function dataProviderForTestRegisterContainerConfiguration() : array
    {
        return [
            ['prod'],
            ['test'],
            ['dev'],
            ['hjdkslhfj'],
        ];
    }
}
