Feature: Show information for job with status new/active/timeout/failed.

    Background: I have a new job in the database.
        Given I have a job with the following data in the database:
            | createdAt           | 2017-02-20 15:16:57     |
            | email               | example@example.com     |
            | fastaLength         | 1788                    |
            | gcRatio             | 30                      |
            | hash                | 85DFFD85-1234-5678-9ABC |
            | length              | 15                      |
            | maxMononucStretches | 4                       |
            | status              | new                     |
            | timeout             | 15                      |

    @db
    Scenario: I get basic information for the new job and the queue position.
        Given I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Results of Job" in the "h1" element
        And   I should see "Status - new" in the "ul:nth-of-type(1) li:nth-child(1)" element
        And   I should see "Position in queue - 1" in the "ul:nth-of-type(1) li:nth-child(2)" element
        And   I should see "Length - 15" in the "ul:nth-of-type(2) li:nth-child(1)" element
        And   I should see "max. Mononuc Stretches - 4" in the "ul:nth-of-type(2) li:nth-child(2)" element
        And   I should see "GC Ratio - 30 %" in the "ul:nth-of-type(2) li:nth-child(3)" element

    @db @fb
    Scenario: I get basic information for the new job and the queue position.
        Given I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Results of Job" in the "h1" element
        And   I should see "Status - new" in the "ul:nth-of-type(1) li:nth-child(1)" element
        And   I should see "Position in queue - 1" in the "ul:nth-of-type(1) li:nth-child(2)" element
        And   I should see "Length - 15" in the "ul:nth-of-type(2) li:nth-child(1)" element
        And   I should see "max. Mononuc Stretches - 4" in the "ul:nth-of-type(2) li:nth-child(2)" element
        And   I should see "GC Ratio - 30 %" in the "ul:nth-of-type(2) li:nth-child(3)" element
        And   I should see "Timeout - 15 h" in the "ul:nth-of-type(2) li:nth-child(4)" element

    @db
    Scenario Outline: I get basic information for the active/timeout/failed job and the start time.
        Given I use <host> host
        And   I update the inserted job with the following:
            | status    | <status>            |
            | startedAt | 2017-02-15 17:41:57 |
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Results of Job" in the "h1" element
        And   I should see "Status - <status>" in the "ul:nth-of-type(1) li:nth-child(1)" element
        And   I should see "Started at - 2017-02-15 17:41:57" in the "ul:nth-of-type(1) li:nth-child(2)" element
        And   I should see "Length - 15" in the "ul:nth-of-type(2) li:nth-child(1)" element
        And   I should see "max. Mononuc Stretches - 4" in the "ul:nth-of-type(2) li:nth-child(2)" element
        And   I should see "GC Ratio - 30 %" in the "ul:nth-of-type(2) li:nth-child(3)" element
        Examples:
            | host    | status  |
            | default | active  |
            | default | failed  |
            | default | timeout |
            | fb      | active  |
            | fb      | failed  |
            | fb      | timeout |

    @db
    Scenario Outline: I get an error for the removed job.
        Given I use <host> host
        And   I update the inserted job with the following:
            | status | removed |
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 404
        And   I should see "Job not found."
        Examples:
            | host    |
            | default |
            | fb      |

    @db
    Scenario Outline: I see a link to the homepage.
        Given I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Homepage" in the "a" element
        When  I follow "Homepage"
        Then  the response status code should be 200
        And   the url should match "/"
        Examples:
            | host    |
            | default |
            | fb      |

    @db @files @javascript
    Scenario Outline: I see the fasta sequence loaded via ajax.
        Given there is a file "1/hyp_up1.fna" with the same content as "fasta/valid.fna"
        And   I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        When  I wait for the overlay to hide
        Then  I should see the content of "fasta/valid.fna" in the ".result--fasta" element
        Examples:
            | host    |
            | default |
            | fb      |
