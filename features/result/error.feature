Feature: Show error pages if job not found.

    @db
    Scenario Outline: I see an error if a job is not found.
        Given I use <host> host
        And   I am on "/result/NOT-FOUND"
        Then  the response status code should be 404
        And   I should see "Job not found."
        Examples:
            | host    |
            | default |
            | fb      |

    @db
    Scenario Outline: I see an error if file download for a non existing job is requested.
        Given I use <host> host
        And   I am on "/result/NOT-FOUND/download/file.name"
        Then  the response status code should be 404
        And   I should see "File not found."
        Examples:
            | host    |
            | default |
            | fb      |

    @db
    Scenario Outline: I see an error if file download with invalid characters is requested.
        Given I use <host> host
        And   I am on "/result/NOT-FOUND/download/file:name"
        Then  the response status code should be 404
        And   I should see "No route found for \"GET /result/NOT-FOUND/download/file:name\""
        But   I should not see "File not found."
        Examples:
            | host    |
            | default |
            | fb      |
