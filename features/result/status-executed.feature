Feature: Show information for job with status executed.

    Background: I have a executed job in the database and the needed files.
        Given I have a job with the following data in the database:
            | createdAt           | 2017-02-20 15:16:57     |
            | email               | example@example.com     |
            | executedAt          | 2017-02-23 20:32:15     |
            | fastaLength         | 1788                    |
            | gcRatio             | 30                      |
            | hash                | 85DFFD85-1234-5678-9ABC |
            | length              | 15                      |
            | maxMononucStretches | 4                       |
            | startedAt           | 2017-02-23 20:58:47     |
            | status              | executed                |
        And   there is a file "1/hyp_up1.csv" with the same content as "job-data/hyp_up1.csv"
        And   there is a file "1/hyp_up1.fna" with the same content as "job-data/hyp_up1.fna"
        And   there is a file "1/hyp_up1_filtered.csv" with the same content as "job-data/hyp_up1_filtered.csv"

    @db
    Scenario Outline: I get basic information for the job.
        Given I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Results of Job" in the "h1" element
        And   I should see "Status - executed" in the "ul:nth-of-type(1) li:nth-child(1)" element
        And   I should see "Started at - 2017-02-23 20:58:47" in the "ul:nth-of-type(1) li:nth-child(2)" element
        And   I should see "Executed at - 2017-02-23 20:32:15" in the "ul:nth-of-type(1) li:nth-child(3)" element
        And   I should see "Available at least until 2017-03-25 20:32:15" in the "ul:nth-of-type(1) li:nth-child(4)" element
        And   I should see "Length - 15" in the "ul:nth-of-type(2) li:nth-child(1)" element
        And   I should see "max. Mononuc Stretches - 4" in the "ul:nth-of-type(2) li:nth-child(2)" element
        And   I should see "GC Ratio - 30 %" in the "ul:nth-of-type(2) li:nth-child(3)" element
        Examples:
            | host    |
            | default |
            | fb      |

    @db
    Scenario Outline: I see a link to the homepage.
        Given I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        And   I should see "Homepage" in the "a" element
        When  I follow "Homepage"
        Then  the response status code should be 200
        And   the url should match "/"
        Examples:
            | host    |
            | default |
            | fb      |

    @db
    Scenario Outline: I can download various files.
        Given I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        When  I follow "<name>"
        Then  the response status code should be 200
        And   the url should match "/result/85DFFD85-1234-5678-9ABC/download/<name>"
        Examples:
            | host    | name                 |
            | default | hyp_up1.csv          |
            | default | hyp_up1.fna          |
            | default | hyp_up1_filtered.csv |
            | fb      | hyp_up1.csv          |
            | fb      | hyp_up1.fna          |
            | fb      | hyp_up1_filtered.csv |

    @db @files @javascript
    Scenario Outline: I see the fasta sequence and table loaded via ajax.
        Given there is a file "1/hyp_up1.fna" with the same content as "job-data/hyp_up1.fna"
        And   I use <host> host
        And   I am on "/result/85DFFD85-1234-5678-9ABC"
        Then  the response status code should be 200
        When  I wait for the overlay to hide
        Then  I should see the content of "job-data/hyp_up1.fna" in the ".result--fasta" element
        And   I should see 38 "table tbody tr" elements
        Examples:
            | host    |
            | default |
            | fb      |
