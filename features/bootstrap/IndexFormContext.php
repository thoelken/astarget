<?php declare(strict_types = 1);

namespace Features;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use DateTime;
use Features\Traits\BaseUrl;
use Features\Traits\Database;
use Features\Traits\Files;
use PHPUnit\Framework\Assert;

class IndexFormContext extends MinkContext implements Context
{
    use KernelDictionary;
    use BaseUrl;
    use Database;
    use Files;

    public function __construct(string $dataPath)
    {
        $this->_setDataPath($dataPath);
    }

    /**
     * Fills form field with content of behat test fixture file.
     *
     * @When I fill in :field from :file
     */
    public function iFillInFrom(string $field, string $path) : void
    {
        $fullPath = $this->_getBehatFilePath($path);

        $this->fillField($field, file_get_contents($fullPath));
    }

    /**
     * Asserts that file in var/data and a behat test fixture file have the same content.
     *
     * @Then I should see the file :file with the same content as :original
     */
    public function iShouldSeeTheFileWithTheSameContentAs(string $file, string $original) : void
    {
        $testFileContent     = file_get_contents($this->_getDataFilePath($file));
        $originalFileContent = file_get_contents($this->_getBehatFilePath($original));

        Assert::assertEquals($originalFileContent, $testFileContent);
    }

    /**
     * Assert job entry in database has given data.
     *
     * @Then I should see the job with id :id with the following data in the database:
     */
    public function iShouldSeeTheJobWithIdWithTheFollowingDataInTheDatabase(int $id, TableNode $table) : void
    {
        $entityManager = $this->_getEntityManager();

        $job = $entityManager
            ->getRepository('AppBundle:Job')
            ->find($id);

        foreach ($table->getTable() as $row) {
            $getter = 'get' . ucfirst($row[0]);
            if ($row[1] === 'NULL') {
                Assert::assertNull($job->{$getter}());
            } elseif ($row[1] === 'NOW()') {
                // Test if diff is less than 10 seconds
                $now = new DateTime('now');
                $dateTime = $job->{$getter}();
                $diff = $dateTime->diff($now);
                Assert::assertLessThanOrEqual(30, $diff->format('%s'));
            } else {
                Assert::assertEquals($row[1], $job->{$getter}());
            }
        }
    }
}
