<?php declare(strict_types = 1);

namespace Features\Traits;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Needs KernelDictionary.
 */
trait Database
{
    /**
     * Create database and execute migrations.
     *
     * @BeforeScenario @db
     */
    public function setUpDatabase() : void
    {
        $application = $this->_createApplication();

        $this->_executeCommand(
            $application,
            [
                'command' => 'doctrine:database:create',
            ]
        );
        $this->_executeCommand(
            $application,
            [
                'command' => 'doctrine:migrations:migrate',
                'version' => 'latest',
            ]
        );
    }

    /**
     * Drop database.
     *
     * @AfterScenario @db
     */
    public function dropDatabase() : void
    {
        $application = $this->_createApplication();

        $this->_executeCommand(
            $application,
            [
                'command' => 'doctrine:database:drop',
                '--force' => true,
            ]
        );
    }

    /**
     * Creates console application.
     *
     * @return Application
     */
    private function _createApplication() : Application
    {
        $kernel = $this->getKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);
        return $application;
    }

    /**
     * Execute command and output stdout if exitCode not 0.
     *
     * @param  Application $application Console application
     * @param  array       $options     Command options
     */
    private function _executeCommand(Application $application, array $options) : void
    {
        $options['--no-interaction'] = true;

        $input = new ArrayInput($options);

        $output   = new BufferedOutput();
        $exitCode = $application->run($input, $output);

        if ($exitCode !== 0) {
            echo $output->fetch();
        }
    }

    private function _getEntityManager() : EntityManager
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
}
