<?php declare(strict_types = 1);

namespace Features\Traits;

/**
 * Needs MinkContext.
 */
trait BaseUrl
{
    /**
     * @BeforeScenario
     */
    public function setDefaultBaseUrl() : void
    {
        putenv('SWITCH_IP_FB');
        $this->setMinkParameter('base_url', 'http://test.astarget.local');
    }

    /**
     * @BeforeScenario @fb
     */
    public function setFbBaseUrl() : void
    {
        putenv('SWITCH_IP_FB=true');
        $this->setMinkParameter('base_url', 'http://test.fb.astarget.local');
    }

    /**
     * @Given I use default host
     */
    public function iUseDefaultHost()
    {
        $this->setDefaultBaseUrl();
    }

    /**
     * @Given I use fb host
     */
    public function iUseFbHost()
    {
        $this->setFbBaseUrl();
    }
}
