<?php declare(strict_types = 1);

namespace Features\Traits;

use PHPUnit\Framework\Assert;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Needs MinkContext, KernelDictionary
 */
trait Files
{
    /**
     * Full path to var/data directory.
     *
     * @var string
     */
    private $_dataPath;

    /**
     * Backups var/data/1 to var/data/backup.
     *
     * @BeforeScenario @files
     */
    public function backupData() : void
    {
        $jobDir = $this->_getDataFilePath('1');
        $backup = $this->_getDataFilePath('backup');

        $filesystem = $this->_getFilesystem();

        if ($filesystem->exists($jobDir)) {
            if ($filesystem->exists($backup)) {
                $filesystem->remove($jobDir);
            } else {
                $filesystem->rename($jobDir, $backup);
            }
        }
    }

    /**
     * Restores backup var/data/backup tp var/data/1.
     *
     * @AfterScenario @files
     */
    public function restoreData() : void
    {
        $jobDir = $this->_getDataFilePath('1');
        $backup = $this->_getDataFilePath('backup');

        $filesystem = $this->_getFilesystem();

        if ($filesystem->exists($jobDir)) {
            $filesystem->remove($jobDir);
        }
        if ($filesystem->exists($backup)) {
            $filesystem->rename($backup, $jobDir);
        }
    }

    /**
     * Prepend behat files path.
     *
     * @param  string $path
     * @return string
     */
    private function _getBehatFilePath(string $path) : string
    {
        return $this->_getFilePath($this->getMinkParameter('files_path'), $path);
    }

    /**
     * Prepend var/data path.
     *
     * @param  string $path
     * @return string
     */
    private function _getDataFilePath(string $path) : string
    {
        return $this->_getFilePath($this->_dataPath, $path);
    }

    /**
     * Concats basePath and path.
     *
     * @param  string $basePath
     * @param  string $path
     * @return string
     */
    private function _getFilePath(string $basePath, string $path) : string
    {
        Assert::assertNotNull($basePath);

        $fullPath = rtrim(
            realpath($basePath),
            DIRECTORY_SEPARATOR
        ) . DIRECTORY_SEPARATOR . $path;

        return $fullPath;
    }

    private function _getFilesystem() : Filesystem
    {
        return $this->getContainer()->get('filesystem');
    }

    private function _setDataPath(string $dataPath) : void
    {
        $this->_dataPath = $dataPath;
    }
}
