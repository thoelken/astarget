Feature: Form for creating jobs has basic parameter validation.

    Scenario: Generate errors when sending form without any values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               |  |
            | index[length]              |  |
            | index[maxMononucStretches] |  |
            | index[gcRatio]             |  |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value should not be blank." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_gcRatio'] + ul li" element

    @fb
    Scenario: Generate errors when sending form without any values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               |  |
            | index[length]              |  |
            | index[maxMononucStretches] |  |
            | index[gcRatio]             |  |
            | index[timeout]             |  |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value should not be blank." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_gcRatio'] + ul li" element
        And   I should see "This value should not be blank." in the "label[for='index_timeout'] + ul li" element


    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | abc |
            | index[maxMononucStretches] | abc |
            | index[gcRatio]             | abc |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_length'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_gcRatio'] + ul li" element

    @fb
    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | abc |
            | index[maxMononucStretches] | abc |
            | index[gcRatio]             | abc |
            | index[timeout]             | abc |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_length'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_gcRatio'] + ul li" element
        And   I should see "This value is not valid." in the "label[for='index_timeout'] + ul li" element


    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | 1   |
            | index[maxMononucStretches] | 0   |
            | index[gcRatio]             | 110 |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should be 4 or more." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should be greater than 0." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should be 100 or less." in the "label[for='index_gcRatio'] + ul li" element

    @fb
    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | -1  |
            | index[maxMononucStretches] | 0   |
            | index[gcRatio]             | 110 |
            | index[timeout]             | 100 |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should be 1 or more." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should be greater than 0." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should be 100 or less." in the "label[for='index_gcRatio'] + ul li" element
        And   I should see "This value should be 96 or less." in the "label[for='index_timeout'] + ul li" element


    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | 25  |
            | index[maxMononucStretches] | 0   |
            | index[gcRatio]             | -1  |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should be 18 or less." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should be greater than 0." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should be 0 or more." in the "label[for='index_gcRatio'] + ul li" element

    @fb
    Scenario: Generate errors when sending form with invalid values
        Given I am on "/"
        Then  the response status code should be 200
        When  I fill in the following:
            | index[email]               | abc |
            | index[length]              | -1  |
            | index[maxMononucStretches] | 0   |
            | index[gcRatio]             | -1  |
            | index[timeout]             | -1  |
        And   I press "Submit"
        Then  the response status code should be 200
        And   the url should match "/"
        Then  I should see "This value is not a valid email address." in the "label[for='index_email'] + ul li" element
        And   I should see "This value should be 1 or more." in the "label[for='index_length'] + ul li" element
        And   I should see "This value should be greater than 0." in the "label[for='index_maxMononucStretches'] + ul li" element
        And   I should see "This value should be 0 or more." in the "label[for='index_gcRatio'] + ul li" element
        And   I should see "This value should be 1 or more." in the "label[for='index_timeout'] + ul li" element
