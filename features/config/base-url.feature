Feature: Base url is configurable with tags for every driver.
        Use "http://test.astarget.local" as default and "http://test.fb.astarget.local" when scenario is annotated with
        the @fb tag. This behaviour should work with the default and javascript-supported (@javascript) driver.

    Scenario: Base url is "http://test.astarget.local" as default
        Given I am on "/"
        Then  the response status code should be 200
        And   the base url should be "http://test.astarget.local"

    @fb
    Scenario: Base url is "http://test.fb.astarget.local" when annotated with @fb
        Given I am on "/"
        Then  the response status code should be 200
        And   the base url should be "http://test.fb.astarget.local"

    @javascript
    Scenario: Base url is "http://test.astarget.local" as default with javascript-supported driver
        Given I am on "/"
        Then  the response status code should be 200
        And   the base url should be "http://test.astarget.local"

    @fb @javascript
    Scenario: Base url is "http://test.fb.astarget.local" when annotated with @fb with javascript-supported driver
        Given I am on "/"
        Then  the response status code should be 200
        And   the base url should be "http://test.fb.astarget.local"
