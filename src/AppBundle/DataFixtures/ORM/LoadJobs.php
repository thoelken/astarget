<?php declare(strict_types = 1);

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Service\Job as JobService;
use DateTime;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadJobs implements FixtureInterface
{
    /**
     * Generates test data for development.
     *
     * @param ObjectManager $manager DAL manager
     */
    public function load(ObjectManager $manager) : void
    {
        $job1 = new JobEntity();
        $job1->setCreatedAt(DateTime::createFromFormat('Y-m-d H:i', '2016-11-01 10:15'));
        $job1->setEmail('test@example.de');
        $job1->setExecutedAt(DateTime::createFromFormat('Y-m-d H:i', '2016-11-02 02:32'));
        $job1->setFastaLength(1788);
        $job1->setGcRatio(40);
        $job1->setHash('85DFFD85-FA8F-4A39-8E47');
        $job1->setLength(16);
        $job1->setMaxMononucStretches(3);
        $job1->setStartedAt(DateTime::createFromFormat('Y-m-d H:i', '2016-11-02 01:48'));
        $job1->setStatus(JobService::STATUS_EXECUTED);

        $job2 = new JobEntity();
        $job2->setCreatedAt(DateTime::createFromFormat('Y-m-d H:i', '2016-11-01 14:15'));
        $job2->setEmail('test@example.de');
        $job2->setFastaLength(1788);
        $job2->setGcRatio(30);
        $job2->setHash('85DFFD85-1234-4A39-8E47');
        $job2->setLength(12);
        $job2->setMaxMononucStretches(5);
        $job2->setStatus(JobService::STATUS_NEW);
        $job2->setTimeout(48);

        $manager->persist($job1);
        $manager->persist($job2);
        $manager->flush();
    }
}
