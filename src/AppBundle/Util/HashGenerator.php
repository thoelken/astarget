<?php declare(strict_types = 1);

namespace AppBundle\Util;

class HashGenerator
{
    /**
     * Generates hash.
     *
     * @return string
     */
    public function generate() : string
    {
        return sprintf(
            '%04X%04X-%04X-%04X-%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151)
        );
    }
}
