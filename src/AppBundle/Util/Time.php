<?php declare(strict_types = 1);

namespace AppBundle\Util;

use DateTime;
use DateTimeZone;

class Time
{
    /**
     * Current timezone.
     */
    const TIMEZONE = 'Europe/Berlin';

    /**
     * Returns a new DateTime instance with local timezone and current timestamp.
     * Simple wrapper to increase testability.
     *
     * @return DateTime
     */
    public function getCurrentDateTime() : DateTime
    {
        return new DateTime(
            'now',
            new DateTimeZone(
                self::TIMEZONE
            )
        );
    }
}
