<?php declare(strict_types = 1);

namespace AppBundle\Util;

use AppBundle\Entity\Job as JobEntity;

class JobTimeout
{
    /**
     * @var int
     */
    private $_jobTimeoutHours;

    public function __construct(int $jobTimeoutHours)
    {
        $this->_jobTimeoutHours = $jobTimeoutHours;
    }

    /**
     * Returns job timeout in hours.
     *
     * @param  JobEntity $job
     * @return int
     */
    public function getTimeoutInHours(JobEntity $job) : int
    {
        return $job->getTimeout() ?: $this->_jobTimeoutHours;
    }

    /**
     * Returns job timeout in seconds.
     *
     * @param  JobEntity $job
     * @return int
     */
    public function getTimeoutInSeconds(JobEntity $job) : int
    {
        return $this->getTimeoutInHours($job) * 3600;
    }
}
