<?php declare(strict_types = 1);

namespace AppBundle\Util;

use AppBundle\Entity\Job as JobEntity;

class Path
{
    const FILENAME_INPUT           = 'hyp_up1.fna';
    const FILENAME_OUTPUT          = 'hyp_up1.csv';
    const FILENAME_OUTPUT_FILTERED = 'hyp_up1_filtered.csv';
    const SCRIPT_GENERATOR         = 'generate_prediction.sh';

    private $_dataDirectory;

    private $_scriptsDirectory;

    public function __construct(string $dataDirectory, string $scriptsDirectory)
    {
        $this->_dataDirectory    = $dataDirectory;
        $this->_scriptsDirectory = $scriptsDirectory;
    }

    /**
     * Generates path to data directory of the job with provided filename.
     *
     * @param  JobEntity $job
     * @param  string    $filename
     * @return string
     */
    public function getFilePath(JobEntity $job, string $filename) : string
    {
        return "{$this->getJobDirectory($job)}/{$filename}";
    }

    /**
     * Generates path to data directory of provided job.
     *
     * @param  JobEntity $job
     * @return string
     */
    public function getJobDirectory(JobEntity $job) : string
    {
        return "{$this->_dataDirectory}/{$job->getId()}";
    }

    /**
     * Returns path to scripts directory.
     *
     * @return string
     */
    public function getScriptsDirectory() : string
    {
        return $this->_scriptsDirectory;
    }

    /**
     * Generates path to provided script.
     *
     * @param  string $filename
     * @return string
     */
    public function getScriptPath(string $filename) : string
    {
        return "{$this->_scriptsDirectory}/{$filename}";
    }
}
