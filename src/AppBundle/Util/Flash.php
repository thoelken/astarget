<?php declare(strict_types = 1);

namespace AppBundle\Util;

use Symfony\Component\HttpFoundation\Session\Session;

class Flash
{
    const TYPE_ERROR   = 'error';
    const TYPE_SUCCESS = 'success';

    const MESSAGE_JOB_CREATED         = 'Your request is currently queued/being processed.<br>'
        . 'Depending on the queue, input length and search parameters, this can take several minutes/hours. '
        . 'Please come back to this site later.';
    const MESSAGE_JOB_NOT_FOUND       = 'Job not found.';
    const MESSAGE_JOB_REMOVED         = 'Job successfully removed.';
    const MESSAGE_REMOVE_FAILED       = 'Job could not be removed. Try again later or contact the admin.';
    const MESSAGE_REMOVE_WHILE_ACTIVE = 'Job is currently executing. It can not be removed during execution.';

    /**
     * @var Session
     */
    private $_session;

    public function __construct(Session $session)
    {
        $this->_session = $session;
    }

    /**
     * Adds message to the flasg bag.
     *
     * @param string $type    Flash key
     * @param string $message Flash message
     */
    public function addFlash(string $type, string $message) : void
    {
        $this->_session
            ->getFlashBag()
            ->add($type, $message);
    }
}
