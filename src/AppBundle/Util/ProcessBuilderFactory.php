<?php declare(strict_types = 1);

namespace AppBundle\Util;

use Symfony\Component\Process\ProcessBuilder;

class ProcessBuilderFactory
{
    /**
     * Returns a new ProcessBuilder instance.
     * Simple wrapper to increase testability.
     *
     * @return ProcessBuilder
     */
    public function getProcessBuilder() : ProcessBuilder
    {
        return new ProcessBuilder();
    }
}
