<?php declare(strict_types = 1);

namespace AppBundle\Util;

class FileReader
{
    /**
     * Reads a file and returns the content.
     * @param string $path  path of the file
     * @return string       string holding the content of the file, null when reading failed
     */
    public function readFile(string $path)
    {
        try {
            $string = file_get_contents($path);
        } catch (\Exception $ex) {
            return null;
        }
        if (empty($string)) {
            return null;
        }

        return $string;
    }
}
