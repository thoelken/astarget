<?php declare(strict_types = 1);

namespace AppBundle\Util;

use Swift_Message;

class MailMessage
{
    /**
     * Returns a Swift_Message instance.
     * Simple wrapper to increase testability.
     *
     * @return Swift_Message
     */
    public function getMessage() : Swift_Message
    {
        return Swift_Message::newInstance();
    }
}
