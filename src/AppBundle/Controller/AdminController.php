<?php declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Service\Job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdminController extends Controller
{
    public function indexAction() : Response
    {
        $authentication = $this->get('util.authentication');

        if (!$authentication->hasAccess()) {
            throw new AccessDeniedHttpException('Access Denied.');
        }

        $jobService = $this->get('service.job');

        return $this->render(
            'admin/index.html.twig',
            [
                'activeJobs'   => $jobService->getByStatus([Job::STATUS_ACTIVE]),
                'defTimeout'   => $this->getParameter('app.job_timeout_hours'),
                'finishedJobs' => $jobService->getByStatus(
                    [Job::STATUS_EXECUTED, Job::STATUS_FAILED, Job::STATUS_TIMEOUT]
                ),
                'newJobs'      => $jobService->getByStatus([Job::STATUS_NEW]),
            ]
        );
    }
}
