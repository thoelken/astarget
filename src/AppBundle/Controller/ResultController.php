<?php declare(strict_types = 1);

namespace AppBundle\Controller;

use AppBundle\Service\Job as JobService;
use AppBundle\Util\Flash;
use AppBundle\Util\Path;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ResultController extends Controller
{
    /**
     * Shows results of job with provided hash
     *
     * @param  string $hash
     * @return Response
     */
    public function indexAction(string $hash) : Response
    {
        $job = $this->get('service.job')->getByHash($hash);

        if ($job === null || $job->getStatus() === JobService::STATUS_REMOVED) {
            throw $this->createNotFoundException('Job not found.');
        }

        $queuePosition = $this->get('service.job')->getQueuePosition($job);

        return $this->render(
            'result/index.html.twig',
            [
                'adminAccess'     => $this->get('util.authentication')->hasAccess(),
                'deleteAfterDays' => $this->getParameter('app.delete_after_days'),
                'job'             => $job,
                'queuePosition'   => $queuePosition,
            ]
        );
    }

    /**
     * Offers file of job with provided job for download.
     *
     * @param  string $hash
     * @param  string $name
     * @return Response
     */
    public function downloadAction(string $hash, string $name) : Response
    {
        $notFoundException = $this->createNotFoundException('File not found.');
        $job               = $this->get('service.job')->getByHash($hash);

        if ($job === null) {
            throw $notFoundException;
        }

        $file = $this->get('util.path')->getFilePath($job, $name);

        if (!$this->get('filesystem')->exists($file)) {
            throw $notFoundException;
        }

        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        return $response;
    }

    /**
     * Removes job with provided hash and redirects with flash message to index route.
     *
     * @param  string $hash
     * @return Response
     */
    public function removeAction(string $hash) : Response
    {
        $job = $this->get('service.job')->getByHash($hash);

        if ($job === null || $job->getStatus() === JobService::STATUS_REMOVED) {
            return $this->_redirectWithFlash(
                Flash::TYPE_ERROR,
                Flash::MESSAGE_JOB_NOT_FOUND
            );
        }

        if ($job->getStatus() === JobService::STATUS_ACTIVE) {
            return $this->_redirectWithFlash(
                Flash::TYPE_ERROR,
                Flash::MESSAGE_REMOVE_WHILE_ACTIVE
            );
        }

        $directoryRemove = $this->get('service.filesystem')->removeJobDirectory($job);

        if (!$directoryRemove) {
            return $this->_redirectWithFlash(
                Flash::TYPE_ERROR,
                Flash::MESSAGE_REMOVE_FAILED
            );
        }

        $job->setStatus(JobService::STATUS_REMOVED);
        $this->get('service.job')->save($job);

        return $this->_redirectWithFlash(
            Flash::TYPE_SUCCESS,
            Flash::MESSAGE_JOB_REMOVED
        );
    }

    private function _redirectWithFlash(string $type, string $message) : Response
    {
        $this->get('util.flash')->addFlash($type, $message);
        return $this->redirectToRoute('index');
    }
}
