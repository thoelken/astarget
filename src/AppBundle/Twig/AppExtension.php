<?php declare(strict_types = 1);

namespace AppBundle\Twig;

use DateTime;
use Twig_Extension;
use Twig_Filter;

class AppExtension extends Twig_Extension
{
    /**
     * @var int
     */
    private $_jobTimeoutHours;

    public function __construct(int $jobTimeoutHours)
    {
        $this->_jobTimeoutHours = $jobTimeoutHours;
    }

    /**
     * Returns custom twig filters.
     *
     * @return Twig_Filter[]
     */
    public function getFilters() : array
    {
        return [
            new Twig_Filter('dateFormat', [$this, 'dateFormatFilter']),
            new Twig_Filter('timeoutFormat', [$this, 'timeoutFormatFilter']),
        ];
    }

    /**
     * Formats DateTime object in an unified form.
     *
     * @param  DateTime $date
     * @return string
     */
    public function dateFormatFilter(DateTime $date) : string
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Formats job timeout. Uses default value if null.
     *
     * @param  int|null $timeout
     * @return string
     */
    public function timeoutFormatFilter($timeout) : string
    {
        if ($timeout === null) {
            $timeout = $this->_jobTimeoutHours;
        }
        return "{$timeout} h";
    }
}
