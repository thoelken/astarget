<?php declare(strict_types = 1);

namespace AppBundle\Form;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Util\Authentication;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

/**
 * @SuppressWarnings(PMD.CouplingBetweenObjects)
 */
class IndexType extends AbstractType
{
    /**
     * @var Authentication
     */
    private $_authentication;

    /**
     * @var int
     */
    private $_minLength;

    /**
     * @var int
     */
    private $_maxLength;

    /**
     * @var int
     */
    private $_defaultLength;

    /**
     * @var int
     */
    private $_defaultStretches;

    /**
     * @var int
     */
    private $_defaultGC;

    /**
     * @var int
     */
    private $_defaultTimeout;

    /**
     * @var int
     */
    private $_maximalTimeout;

    public function __construct(
        Authentication $authentication,
        int $minLength,
        int $maxLength,
        int $defLength,
        int $defStretches,
        int $defGC,
        int $defTimeout,
        int $maxTimeout
    ) {
        $this->_authentication = $authentication;
        $this->_minLength = $minLength;
        $this->_maxLength = $maxLength;
        $this->_defaultLength = $defLength;
        $this->_defaultStretches = $defStretches;
        $this->_defaultGC = $defGC;
        $this->_defaultTimeout = $defTimeout;
        $this->_maximalTimeout = $maxTimeout;
    }

    /**
     * @SuppressWarnings(PMD.UnusedFormalParameter)
     */
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $admin = $this->_authentication->hasAccess();

        $builder
            ->add('uploadfilechoice', ChoiceType::class, [
                'choices'  => [
                    'Enter Text'  => false,
                    'Upload File' => true,
                ],
                'label'  => 'Type of input',
                'mapped' => false,
            ])
            ->add('fasta', TextareaType::class, [
                'label'    => 'FASTA',
                'mapped'   => false,
                'required' => false,
            ])
            ->add('fastafile', FileType::class, [
                'label'    => 'File',
                'mapped'   => false,
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Type([
                        'type' => 'String',
                    ]),
                    new Email(),
                ],
                'label' => 'Mail',
            ])
            ->add('length', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Type([
                        'type' => 'integer',
                    ]),
                    new Range([
                        'min' => $admin ? 1 : $this->_minLength,
                        'max' => $admin ? PHP_INT_MAX : $this->_maxLength,
                    ])
                ],
                'data'  => $this->_defaultLength,
                'label' => 'Length',
            ])
            ->add('maxMononucStretches', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Type([
                        'type' => 'integer',
                    ]),
                    new GreaterThan([
                        'value' => 0,
                    ]),
                ],
                'data'  => $this->_defaultStretches,
                'label' => 'max. Mononuc Stretches',
            ])
            ->add('gcRatio', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Type([
                        'type' => 'integer',
                    ]),
                    new Range([
                        'min' => 0,
                        'max' => 100,
                    ]),
                ],
                'data'  => $this->_defaultGC,
                'label' => 'min. GC Ratio [%]',
            ]);

        if ($admin) {
            $builder
                ->add('timeout', IntegerType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Type([
                            'type' => 'integer',
                        ]),
                        new Range([
                            'min' => 1,
                            'max' => $this->_maximalTimeout,
                        ])
                    ],
                    'data'  => $this->_defaultTimeout,
                    'label' => 'Timeout [h]',
                ]);
        }

        $builder
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver) : void
    {
        $resolver->setDefaults(
            [
                'data_class' => JobEntity::class,
            ]
        );
    }
}
