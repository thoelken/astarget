/* Modified Original:
 *
 * jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */

(function ($) {
    "use strict";

    var object = $({});

    $.subscribe = function (name, callback) {
        object.on(name, function () {
            Array.prototype.shift.call(arguments, 1);
            callback.apply(null, arguments);
        });
    };

    $.publish = function (name, data) {
        object.trigger(name, data);
    };

}(jQuery));
