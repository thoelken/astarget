/**
 * Index-form module
 *
 * Loads fasta from url.
 */
(function ($) {
    "use strict";

    activate();

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Initialize form input toggle.
         */
        $.subscribe("index-form.init", function () {
            handleInputDisplay();
            $("#index_uploadfilechoice").on("change", handleInputDisplay);
        });
    }

    /**
     * Displays the chosen input field and hides the other one.
     */
    function handleInputDisplay() {
        $("form > div").removeClass("hidden");

        if ($("#index_uploadfilechoice").val() === "1") {
            $("form > div.fastatext").addClass("hidden");
        } else {
            $("form > div.fastafile").addClass("hidden");
        }
    }

}(jQuery));
