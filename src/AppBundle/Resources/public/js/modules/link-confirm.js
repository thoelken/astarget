/**
 * Link-confirm module
 *
 * Provides functionality to show and hide result overlay.
 */
(function ($) {
    "use strict";

    activate();

    var confirmSelector = ".confirm";
    var duration        = 100;
    var linkSelector    = "a[data-confirm]";

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Init position marker.
         */
        $.subscribe("link-confirm.init", function () {
            init();
        });
    }

    function fadeOut() {
        $(confirmSelector).fadeOut(duration);
    }

    function init() {
        $(document).click(function () {
            fadeOut();
        });

        $(confirmSelector).click(function (event) {
            event.stopPropagation();
        });

        $(linkSelector).click(function () {
            var action = $(this).text();
            var url    = $(this).attr("href");

            $(confirmSelector).find(".confirm--action").attr("href", url).text(action);
            $(confirmSelector).find(".confirm--cancel").click(fadeOut);
            $(confirmSelector).fadeIn(duration);

            return false;
        });
    }

}(jQuery));
