/**
 * Result-table module
 *
 * Manages table in result page.
 */
(function ($) {
    "use strict";

    activate();

    var tableTbody = ".result--table tbody";
    var tableData;
    var tableDataUnfiltered;
    var sortBy;

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Filter result table.
         *
         * @param {Array} filterData Index of column
         */
        $.subscribe("result-table.filter", function (filterData) {
            if (tableData === undefined) {
                return;
            }
            filter(filterData);
        });

        /**
         * Init result table.
         *
         * @param {Array} tableData Table data
         */
        $.subscribe("result-table.init", function (data) {
            tableData = data;
            if (tableData !== undefined) {
                tableDataUnfiltered = tableData.slice(0);
                render();
            }
        });

        /**
         * Sort result table.
         *
         * @param {int} index Index of column
         * @param {int} order Order value -1 or 1
         */
        $.subscribe("result-table.sort", function (index, order) {
            if (tableData === undefined) {
                return;
            }
            sortBy = { index : index, order : order };
            sort();
        });
    }

    /**
     * Execute callback asynchronous to prevent ui render blockings and toggles overlay.
     *
     * @param {Function} callback Asynchronous task
     */
    function defer(callback) {
        $.publish("overlay.show");

        setTimeout(function () {
            callback();
            $.publish("overlay.hide");
        }, 0);
    }

    /**
     * Filter result table.
     *
     * @param {Array} filterData Filter data
     */
    function filter(filterData) {
        defer(function () {
            tableData = tableDataUnfiltered.filter(function (row) {
                return !isHidden(filterData, row);
            });

            sort();
        });
    }

    /**
     * Checks if row should be hidden with provided filter data.
     *
     * @param  {Array} filterData Filter data
     * @param  {Array} row        Row data
     * @return {bool}             True if row should be hidden
     */
    function isHidden(filterData, row) {
        for (var i = 0; i < filterData.length; i++) {
            var value = row[filterData[i].index].value;
            var hidden;

            if (filterData[i].type === "min") {
                hidden = value < filterData[i].value;
            } else {
                hidden = value > filterData[i].value;
            }

            if (hidden) {
                return true;
            }
        }
        return false;
    }

    /**
     * Renders table tbody.
     */
    function render() {
        defer(function () {
            $(tableTbody).empty();

            for (var i = 0; i < tableData.length; i++) {
                var row = $("<tr/>");

                for (var j = 0; j < tableData[i].length; j++) {
                    var cell = $("<td>" + tableData[i][j].rawValue + "</td>");
                    row.append(cell);
                }
                $(tableTbody).append(row);
            }

            $.publish("position-marker.init");
        });
    }

    /**
     * Sort table with sortBy criteria.
     */
    function sort() {
        defer(function () {
            if (sortBy !== undefined) {
                tableData.sort(function (a, b) {
                    var valueA = a[sortBy.index].value;
                    var valueB = b[sortBy.index].value;

                    return valueA === valueB ? 0 : (
                        valueA > valueB ? sortBy.order : -1 * sortBy.order
                    );
                });
            }

            render();
        });
    }

}(jQuery));
