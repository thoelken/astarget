/**
 * Table-filter module
 *
 * Sets listener for table filtering and fires events.
 */
(function ($) {
    "use strict";

    activate();

    var inputs = ".result--table input";

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Load fasta.
         */
        $.subscribe("table-filter.init", function () {
            init();
        });
    }

    /**
     * Initializes DOM event handler.
     */
    function init() {
        /**
         * Send filter on enter.
         */
        $(inputs).keypress(function (event) {
            if (event.which === 13) {
                sendFilter();
            }
        });
    }

    /**
     * Publish filter event with aggregated filters.
     */
    function sendFilter() {
        var data = [];

        $(inputs).each(function () {
            if ($(this).val() === "") {
                return;
            }

            data.push({
                index : $(this).data("index"),
                type  : $(this).data("type"),
                value : $(this).val(),
            });
        });

        $.publish("result-table.filter", [data]);
    }

}(jQuery));
