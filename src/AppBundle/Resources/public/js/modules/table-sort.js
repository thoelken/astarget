/**
 * Table-sort module
 *
 * Sets on click listener for table sorting and fires events.
 */
(function ($) {
    "use strict";

    activate();

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Init table sort.
         */
        $.subscribe("table-sort.init", function () {

            $(".result--table").on("click", "th", function () {
                sort($(this));
            });

        });
    }

    /**
     * Sets order value and icons class of th.
     *
     * @param {Object} th    jQuery object of th
     * @param {int}    order Order value -1, 0 or 1
     */
    function setThState(th, order) {
        var classes = "fa fa-sort";

        if (order === 1) {
            classes += "-asc";
        }
        if (order === -1) {
            classes += "-desc";
        }

        th.data("order", order);
        th.find("i").removeClass().addClass(classes);
    }

    /**
     * Fires sort event and setd icon class.
     *
     * @param {Object} element jQuery object of th
     */
    function sort(element) {
        // read attributes
        var order = element.data("order") === 1 ? -1 : 1;
        var index = element.index();

        // set th states
        $(".result--table th").each(function () {
            setThState($(this), 0);
        });
        setThState(element, order);

        $.publish("result-table.sort", [index, order]);
    }

}(jQuery));
