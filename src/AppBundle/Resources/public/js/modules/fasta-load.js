/**
 * Fasta-load module
 *
 * Loads fasta from url.
 */
(function ($) {
    "use strict";

    activate();

    var fastaInputUrl;

    /**
     * Constructor of module.
     * Subscribe to public events.
     */
    function activate() {
        /**
         * Load fasta.
         */
        $.subscribe("fasta-load.load", function () {
            if (fastaInputUrl !== undefined) {
                load();
            }
        });

        /**
         * Save fastaInputUrl.
         */
        $.subscribe("view.fastaInputUrl", function (data) {
            fastaInputUrl = data;
        });
    }

    /**
     * Load fasta.
     */
    function load() {
        $.publish("overlay.show");

        $(".result--fasta").load(fastaInputUrl, function () {
            $.publish("overlay.hide");
        });
    }

}(jQuery));
