<?php declare(strict_types = 1);

namespace AppBundle\Command;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Service\Job as JobService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteJobCommand extends Command
{
    const LOG_PREFIX = 'ExecuteJobCommand';

    /**
     * Configure required credentials for execute-job command.
     */
    protected function configure() : void
    {
        $this
            ->setName('app:execute-job')
            ->setDescription('Executes the next job in queue.')
            ->setHelp('This command executes the next job in queue.');
    }

    /**
     * Executes next job in pipeline if available and sends feedback.
     *
     * @param  InputInterface  $input  Currently unused
     * @param  OutputInterface $output Currently unused
     * @return int                     Exit code of command
     *
     * @SuppressWarnings(PMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $container = $this->getContainer();

        $jobService     = $container->get('service.job');
        $mailService    = $container->get('service.mail');
        $processService = $container->get('service.process');
        $timeUtil       = $container->get('util.time');

        // Check if maximal concurrent running jobs is smaller than currently active jobs
        $countActive = $jobService->countByStatus(JobService::STATUS_ACTIVE);
        if ($countActive >= $this->getContainer()->getParameter('app.max_concurrent_jobs')) {
            $this->_logInfo("No job started. {$countActive} jobs already running.");
            return 0;
        }

        // Check if there is a next job to execute
        $job = $jobService->getNext();
        if (!$job instanceof JobEntity) {
            $this->_logInfo('No job started. No new jobs available.');
            return 0;
        }

        // Set job to active
        $job->setStatus(JobService::STATUS_ACTIVE);
        $job->setStartedAt($timeUtil->getCurrentDateTime());
        $jobService->save($job);
        $this->_logInfo('Starting job execution.', $job);

        // Command execution
        $process = $processService->generatePrediction($job);

        // Update job entity according to exit code of process
        if ($process === null) {
            $job->setStatus(JobService::STATUS_TIMEOUT);
            $this->_logError('Job execution timed out.', $job);
            $mailService->sendNotifyJobTimeout($job);
        } elseif ($process->isSuccessful()) {
            $job->setStatus(JobService::STATUS_EXECUTED);
            $job->setExecutedAt($timeUtil->getCurrentDateTime());
            $this->_logInfo('Successful job execution.', $job);
        } else {
            $job->setStatus(JobService::STATUS_FAILED);
            $this->_logError('Error in job execution.', $job, $process->getErrorOutput());
            $mailService->sendNotifyJobFailed($job, $process->getErrorOutput());
        }

        // Persist job
        $jobService->save($job);

        // Send feedback mail
        $mailService->sendFeedback($job);

        return 0;
    }

    /**
     * Log error level message with additional information.
     *
     * @param string      $message     Log message
     * @param JobEntity   $job         Current job
     * @param string|null $errorOutput Process standard error output
     */
    private function _logError(string $message, JobEntity $job, string $errorOutput = null) : void
    {
        $args = ['job-id' => $job->getId()];

        if ($errorOutput !== null) {
            $args['stderr'] = $errorOutput;
        }

        $this->_getLogger()->error($this->_buildLogMessage($message), $args);
    }

    /**
     * {@inheritDoc}
     */
    protected function _getLogPrefix() : string
    {
        return self::LOG_PREFIX;
    }
}
