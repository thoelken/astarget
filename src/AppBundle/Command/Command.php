<?php declare(strict_types = 1);

namespace AppBundle\Command;

use AppBundle\Entity\Job as JobEntity;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class Command extends ContainerAwareCommand
{
    /**
     * Build log message with current logging prefix.
     *
     * @param  string $message Log message
     * @return string          Prefixed log message
     */
    protected function _buildLogMessage(string $message) : string
    {
        return $this->_getLogPrefix() . ": {$message}";
    }

    /**
     * @return LoggerInterface
     */
    protected function _getLogger() : LoggerInterface
    {
        return $this->getContainer()->get('logger');
    }

    /**
     * Log info level message.
     *
     * @param string    $message Log message
     * @param JobEntity $job     Current job
     */
    protected function _logInfo(string $message, JobEntity $job = null) : void
    {
        $infos = [];

        if ($job !== null) {
            $infos['job-id'] = $job->getId();
        }

        $this->_getLogger()
            ->info(
                $this->_buildLogMessage($message),
                $infos
            );
    }

    /**
     * Returns log prefix.
     *
     * @return string
     */
    abstract protected function _getLogPrefix() : string;
}
