<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20161112180123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('gc_ratio', ['type' => Type::getType('integer')]);
        $table->addColumn('status', 'string');
        $table->addColumn('executed_at', 'datetime');
        $table->addColumn('execution_time', 'integer');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->changeColumn('gc_ratio', ['type' => Type::getType('decimal'), 'precision' => 4, 'scale' => 3]);
        $table->dropColumn('status');
        $table->dropColumn('executed_at');
        $table->dropColumn('execution_time');
    }
}
