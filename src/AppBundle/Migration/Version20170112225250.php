<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20170112225250 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->dropColumn('execution_time');
        $table->addColumn('hash', 'string', ['notnull' => false]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->addColumn('execution_time', 'integer', ['notnull' => false]);
        $table->dropColumn('hash');
    }
}
