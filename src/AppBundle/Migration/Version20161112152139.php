<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20161112152139 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('jobs');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('sequence', 'text', ['length' => 65535]);
        $table->addColumn('email', 'string');
        $table->addColumn('gc_ratio', 'decimal', ['precision' => 4, 'scale' => 3]);
        $table->addColumn('max_ns', 'integer');
        $table->addColumn('created_at', 'datetime');
        $table->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $schema->dropTable('jobs');
    }
}
