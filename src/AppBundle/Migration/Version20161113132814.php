<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20161113132814 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->dropColumn('sequence');
        $table->dropColumn('max_ns');

        $table->addColumn('length', 'integer');
        $table->addColumn('max_mononuc_stretches', 'integer');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->dropColumn('length');
        $table->dropColumn('max_mononuc_stretches');

        $table->addColumn('sequence', 'text', ['length' => 65535]);
        $table->addColumn('max_ns', 'integer');
    }
}
