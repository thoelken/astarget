<?php declare(strict_types = 1);

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PMD.ShortMethodNames)
 */
class Version20170111141416 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->addColumn('fasta_length', 'integer', ['default' => 0]);
        $table->addColumn('started_at', 'datetime', ['notnull' => false]);
        $table->addColumn('timeout', 'integer', ['notnull' => false]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $table = $schema->getTable('jobs');
        $table->dropColumn('fasta_length');
        $table->dropColumn('started_at');
        $table->dropColumn('timeout');
    }
}
