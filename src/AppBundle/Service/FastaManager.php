<?php declare(strict_types = 1);

namespace AppBundle\Service;

use AppBundle\Entity\Job;
use AppBundle\Service\Filesystem;
use AppBundle\Util\Path;
use Symfony\Component\Form\Form;

class FastaManager
{
    const FASTA_OK       = 0;
    const FASTA_INVALID  = 1;
    const FASTA_TOO_LONG = 2;

    /**
     * @var Filesystem
     */
    private $_filesystem;

    /**
     * @var Path
     */
    private $_path;

    /**
     * @var int
     */
    private $_maxFastaLength;

    public function __construct(Filesystem $filesystem, Path $path, int $maxFastaLength)
    {
        $this->_filesystem = $filesystem;
        $this->_path = $path;
        $this->_maxFastaLength = $maxFastaLength;
    }

    /**
     * Retrieves fasta from a given form.
     *
     * @param Form  $form  form to retrieve the data from
     * @return if possible, fasta string is returned, null otherwise
     */
    public function getFormFasta(Form $form)
    {
        if ($form->get('uploadfilechoice')->getData()) {
            $fastafiledata = $form->get('fastafile')->getData();
            if ($fastafiledata != null) {
                return $this->_filesystem->readFile($fastafiledata->getPathname());
            }

            return null;
        }

        return $form->get('fasta')->getData();
    }

    /**
     * Writes a given FASTA string to the directory of the job
     *
     * @param string $fasta  fasta to be written
     * @param Job    $job    Job that holds the fasta's id
     * @return true if file was written, false otherwise
     */
    public function writeFastaToFile(string $fasta, Job $job) : bool
    {
        if ($this->_filesystem->addJobDirectory($job)) {
            $filePath = $this->_path->getFilePath($job, Path::FILENAME_INPUT);
            return $this->_filesystem->writeFile(
                $filePath,
                $fasta
            );
        }

        return false;
    }

    /**
     * Changes FASTA Input to meet the requirements (only upper case, 'U' instead of 'T',
     * no new lines in FASTA) but does not check if it is valid.
     * Returns the corrected FASTA Input.
     *
     * @param  string    $fastaString
     * @return string
     */
    public function correctInput(string $fastaString) : string
    {
        $newString = "";

        // find all types of new lines and fix them to \n (order is important)
        $fastaString = str_replace("\r\n", "\n", $fastaString);
        $fastaString = str_replace("\r", "\n", $fastaString);

        $strArray = explode("\n", $fastaString);

        foreach ($strArray as $line) {
            if ($this->_getFirstChar($line) != '>' && $this->_getFirstChar($line) != ';') {
                $line = strtoupper($line);
                $line = str_replace('U', 'T', $line);
            } else {
                // add new line after headline and comment
                $line .= "\n";
            }
            $newString .= $line;
        }

        $newString .= "\n";

        return $newString;
    }

    /**
     * Returns the length of a given Fasta.
     *
     * @param  string    $fastaString
     * @return int
     */
    public function getFastaLength(string $fastaString) : int
    {
        $strArray = explode("\n", $fastaString);
        foreach ($strArray as $line) {
            if ($this->_getFirstChar($line) != '>' && $this->_getFirstChar($line) != ';') {
                return mb_strlen($line);
            }
        }

        return 0;
    }

    /**
     * Checks FASTA input and returns (error) code for validation.
     *
     * @param  string    $fastaString
     * @return int
     */
    public function checkFasta(string $fastaString) : int
    {
        if (empty($fastaString)) {
            return self::FASTA_INVALID;
        }

        if ($this->getFastaLength($fastaString) > $this->_maxFastaLength) {
            return self::FASTA_TOO_LONG;
        }

        $strArray = explode("\n", $fastaString);

        if (count($strArray) === 1) {
            return self::FASTA_INVALID;
        } else {
            return ($this->_checkHeadLine($strArray)) ? self::FASTA_OK : self::FASTA_INVALID;
        }
    }

    private function _checkHeadLine(array $array) : bool
    {
        if (preg_match("/^[>][A-Za-z0-9][^ÄÜÖäüö>]*$/", $array[0]) === 1) {
            return $this->_checkCommentLine(array_slice($array, 1));
        }
        return false;
    }

    private function _checkCommentLine(array $array) : bool
    {
        if (count($array) == 0) {
            return false;
        }

        if ($this->_getFirstChar($array[0]) == ';') {
            return $this->_checkCommentLine(array_slice($array, 1));
        }

        // function correctInput secures that this is only one line if FASTA is valid
        return $this->_checkFastaLine($array[0]);
    }

    private function _checkFastaLine(string $string) : bool
    {
        return preg_match("/^[ACGT]+$/", $string) === 1;
    }

    private function _getFirstChar(string $string) : string
    {
        return str_split($string)[0];
    }
}
