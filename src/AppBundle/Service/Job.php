<?php declare(strict_types = 1);

namespace AppBundle\Service;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Util\HashGenerator;
use AppBundle\Util\Time;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Form;

class Job
{
    const REPOSITORY      = 'AppBundle:Job';

    const STATUS_ACTIVE   = 'active';
    const STATUS_EXECUTED = 'executed';
    const STATUS_FAILED   = 'failed';
    const STATUS_NEW      = 'new';
    const STATUS_REMOVED  = 'removed';
    const STATUS_TIMEOUT  = 'timeout';

    /**
     * @var EntityManager
     */
    private $_entityManager;

    /**
     * @var HashGenerator
     */
    private $_hashGenerator;

    /**
     * @var Time
     */
    private $_time;

    /**
     * @var int
     */
    private $_deleteAfterDays;

    public function __construct(
        EntityManager $entityManager,
        HashGenerator $hashGenerator,
        Time $time,
        int $deleteAfterDays
    ) {
        $this->_entityManager   = $entityManager;
        $this->_hashGenerator   = $hashGenerator;
        $this->_time            = $time;
        $this->_deleteAfterDays = $deleteAfterDays;
    }

    /**
     * Returns count of all Jobs with provided status.
     *
     * @param  string $status Status to search for
     * @return int            Count of jobs with provided status
     */
    public function countByStatus(string $status) : int
    {
        return count(
            $this->_entityManager
                ->getRepository(self::REPOSITORY)
                ->findBy(['status' => $status])
        );
    }

    /**
     * Fills job with missing values and saves it afterwards.
     *
     * @param Form $form Form that holds the job that is going to be completed
     * @return JobEntity completed Job
     */
    public function createJob(Form $form) : JobEntity
    {
        // fill the object with the form values
        $job = $form->getData();

        // add missing values and save
        $job->setCreatedAt($this->_time->getCurrentDateTime());
        $job->setStatus(self::STATUS_NEW);
        $job->setHash($this->_hashGenerator->generate());

        return $job;
    }

    /**
     * Returns job with provided hash.
     *
     * @param  string $hash   Hash of job
     * @return JobEntity|null Job with provided id
     */
    public function getByHash(string $hash)
    {
        return $this->_entityManager
            ->getRepository(self::REPOSITORY)
            ->findOneBy(['hash' => $hash]);
    }

    /**
     * Returns all jobs with any of the provided status
     *
     * @param  array $status List of status to filter by
     * @return JobEntity[]
     */
    public function getByStatus(array $status) : array
    {
        $queryBuilder = $this->_entityManager
            ->getRepository(self::REPOSITORY)
            ->createQueryBuilder('job');

        $queryBuilder->where(
            $queryBuilder->expr()->in('job.status', ':status')
        );

        $queryBuilder->setParameter('status', $status);

        $queryBuilder->orderBy('job.createdAt', 'ASC');

        $query = $queryBuilder->getQuery();
        return $query->execute();
    }

    /**
     * Returns next job that has to be executed, if a job is in the pipeline.
     *
     * @return JobEntity|null Next job or null if no one is in the pipeline
     */
    public function getNext()
    {
        return $this->_entityManager
            ->getRepository(self::REPOSITORY)
            ->findOneBy(
                ['status'    => self::STATUS_NEW],
                ['createdAt' => 'asc']
            );
    }

    /**
     * Returns position in queue.
     *
     * @param  JobEntity $job
     * @return int
     */
    public function getQueuePosition(JobEntity $job) : int
    {
        $queryBuilder = $this->_entityManager->createQueryBuilder();

        $queryBuilder->select('COUNT(job.id)');
        $queryBuilder->from(self::REPOSITORY, 'job');

        $queryBuilder->where('job.status = :status');
        $queryBuilder->andWhere('job.createdAt <= :time');

        $queryBuilder->setParameter('status', self::STATUS_NEW);
        $queryBuilder->setParameter('time', $job->getCreatedAt());

        $query = $queryBuilder->getQuery();
        return (int) $query->getSingleScalarResult();
    }

    /**
     * Returns all jobs that should be removed because they are executed, failed or timed out and
     * older than the configured property deleteAfterDays.
     *
     * @return JobEntity[] List of removable jobs
     */
    public function getRemovables() : array
    {
        $deleteTime = $this->_time
            ->getCurrentDateTime()
            ->modify("-{$this->_deleteAfterDays} days");

        $queryBuilder = $this->_entityManager
            ->getRepository(self::REPOSITORY)
            ->createQueryBuilder('job');

        $expr = $queryBuilder->expr();

        $queryBuilder->where(
            $expr->orX(
                $expr->andX(
                    $expr->eq('job.status', ':executedStatus'),
                    $expr->lte('job.executedAt', ':time')
                ),
                $expr->andX(
                    $expr->in('job.status', ':notExecutedStatus'),
                    $expr->lte('job.startedAt', ':time')
                )
            )
        );

        $queryBuilder->setParameter('executedStatus', self::STATUS_EXECUTED);
        $queryBuilder->setParameter('notExecutedStatus', [self::STATUS_FAILED, self::STATUS_TIMEOUT]);
        $queryBuilder->setParameter('time', $deleteTime);

        $query = $queryBuilder->getQuery();
        return $query->execute();
    }

    /**
     * Saves job in database.
     *
     * @param JobEntity $job Job that has to be saved
     */
    public function save(JobEntity $job) : void
    {
        $this->_entityManager->persist($job);
        $this->_entityManager->flush();
    }

    /**
     * Removes already saved job from database.
     *
     * @param JobEntity $job Job that has to be removed
     */
    public function remove(JobEntity $job)
    {
        $this->_entityManager->remove($job);
        $this->_entityManager->flush();
    }
}
