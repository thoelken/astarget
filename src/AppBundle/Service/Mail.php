<?php declare(strict_types = 1);

namespace AppBundle\Service;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Util\MailMessage;
use AppBundle\Util\JobTimeout;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\TwigBundle\TwigEngine;

class Mail
{
    /**
     * @var Swift_Mailer
     */
    private $_mailer;

    /**
     * @var MailMessage
     */
    private $_mailMessage;

    /**
     * @var TwigEngine
     */
    private $_twigEngine;

    /**
     * @var string
     */
    private $_adminMailAddress;

    /**
     * @var int
     */
    private $_deleteAfterDays;

    /**
     * @var string
     */
    private $_senderMailAddress;

    public function __construct(
        Swift_Mailer $mailer,
        MailMessage $mailMessage,
        TwigEngine $twigEngine,
        string $adminMailAddress,
        int $deleteAfterDays,
        string $senderMailAddress
    ) {
        $this->_mailer            = $mailer;
        $this->_mailMessage       = $mailMessage;
        $this->_twigEngine        = $twigEngine;
        $this->_adminMailAddress  = $adminMailAddress;
        $this->_deleteAfterDays   = $deleteAfterDays;
        $this->_senderMailAddress = $senderMailAddress;
    }

    /**
     * Sends feedback mail after the provided job has been executed or failed.
     * See status property of job for information if failed or successful.
     *
     * @param JobEntity $job Executed or failed job
     */
    public function sendFeedback(JobEntity $job) : void
    {
        $this->_sendMail(
            'Feedback to Job',
            $job->getEmail(),
            'mail/feedback.txt.twig',
            [
                'deleteAfterDays' => $this->_deleteAfterDays,
                'job'             => $job,
            ]
        );
    }

    /**
     * Sends notify mail to admin if job execution time exceeded configured threshold.
     *
     * @param JobEntity $job         Failed job
     * @param string    $errorOutput Process error output
     */
    public function sendNotifyJobFailed(JobEntity $job, string $errorOutput) : void
    {
        $this->_sendMail(
            'Job failed',
            $this->_adminMailAddress,
            'mail/jobFailed.txt.twig',
            [
                'errorOutput' => $errorOutput,
                'job'         => $job,
            ]
        );
    }

    /**
     * Sends notify mail to admin if job execution timed out.
     *
     * @param JobEntity $job Executed job
     */
    public function sendNotifyJobTimeout(JobEntity $job) : void
    {
        $this->_sendMail(
            'Job timed out',
            $this->_adminMailAddress,
            'mail/jobTimeout.txt.twig',
            [
                'job' => $job,
            ]
        );
    }

    /**
     * Send mail with provided options.
     *
     * @param  string $subject    Subject
     * @param  string $toAddress  Send to address
     * @param  string $template   Twig template path
     * @param  array  $parameters Parameters for view
     */
    private function _sendMail(string $subject, string $toAddress, string $template, array $parameters) : void
    {
        $message = $this->_mailMessage->getMessage();

        $message->setSubject($subject);
        $message->setFrom($this->_senderMailAddress);
        $message->setTo($toAddress);
        $message->setBody(
            $this->_twigEngine->render(
                $template,
                $parameters
            ),
            'text/plain'
        );

        $this->_mailer->send($message);
    }
}
