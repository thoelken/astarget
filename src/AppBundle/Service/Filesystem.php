<?php declare(strict_types = 1);

namespace AppBundle\Service;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Util\FileReader;
use AppBundle\Util\Path;
use Closure;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;
use Symfony\Component\Finder\Finder;

class Filesystem
{
    /**
     * @var SymfonyFilesystem
     */
    private $_filesystem;

    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @var FileReader
     */
    private $_filereader;

    /**
     * @var Path
     */
    private $_path;

    public function __construct(
        SymfonyFilesystem $filesystem,
        LoggerInterface $logger,
        FileReader $filereader,
        Path $path
    ) {
        $this->_filesystem = $filesystem;
        $this->_logger     = $logger;
        $this->_filereader = $filereader;
        $this->_path       = $path;
    }

    private function _handleFilesystem(Closure $closure, string $message) : bool
    {
        try {
            $closure();
        } catch (IOException $exception) {
            $this->_logger->error(
                "An error occurred while {$message} at {$exception->getPath()}",
                ['exception' => $exception]
            );
            return false;
        }

        return true;
    }

    /**
     * Add job directory of provided job.
     *
     * @param  JobEntity $job Job of which the directory should be added
     * @return boolean        true if successful, false otherwise
     */
    public function addJobDirectory(JobEntity $job) : bool
    {
        return $this->_handleFilesystem(
            function () use ($job) {
                $this->_filesystem
                    ->mkdir($this->_path->getJobDirectory($job));
            },
            "adding a directory"
        );
    }

    /**
     * Remove job directory of provided job.
     *
     * @param  JobEntity $job Job of which the directory should be removed
     * @return boolean        true if successful, false otherwise
     */
    public function removeJobDirectory(JobEntity $job) : bool
    {
        return $this->_handleFilesystem(
            function () use ($job) {
                $this->_filesystem->remove($this->_path->getJobDirectory($job));
            },
            "removing a directory"
        );
    }

    /**
     * Writes a string into a file at a given path.
     *
     * @param  string $path     path of the file that is going to be created
     * @param  string $content  string that is going to be written
     * @return boolean          true if successful, false otherwise
     */
    public function writeFile(string $path, string $content) : bool
    {
        return $this->_handleFilesystem(
            function () use ($path, $content) {
                $this->_filesystem->dumpFile($path, $content);
            },
            "creating a file"
        );
    }

    /**
     * Removes a file at a given path.
     *
     * @param  string $path     path of the file that is going to be removed
     * @return boolean          true if successful, false otherwise
     */
    public function removeFile(string $path) : bool
    {
        return $this->_handleFilesystem(
            function () use ($path) {
                $this->_filesystem->remove($path);
            },
            "removing a file"
        );
    }

    /**
     * Reads a file at a given path and returns its content.
     *
     * @param  string $path     path of the file that is going to be read
     * @return string           content of the file, null if empty or any error
     */
    public function readFile(string $path)
    {
        return $this->_filereader->readFile($path);
    }
}
