<?php declare(strict_types = 1);

namespace AppBundle\Service;

use AppBundle\Entity\Job as JobEntity;
use AppBundle\Util\JobTimeout;
use AppBundle\Util\Path;
use AppBundle\Util\ProcessBuilderFactory;
use Symfony\Component\Process\Process as ProcessComponent;
use Symfony\Component\Process\Exception\ProcessTimedOutException;

class Process
{
    /**
     * @var Path
     */
    private $_path;

    /**
     * @var ProcessBuilderFactory
     */
    private $_processBuilderFactory;

    /**
     * @var JobTimeout
     */
    private $_jobTimeoutUtil;

    public function __construct(Path $path, ProcessBuilderFactory $processBuilderFactory, JobTimeout $jobTimeoutUtil)
    {
        $this->_path                  = $path;
        $this->_processBuilderFactory = $processBuilderFactory;
        $this->_jobTimeoutUtil        = $jobTimeoutUtil;
    }

    /**
     * Generates AS Target prediction for provided job.
     *
     * @param  JobEntity $job        Job to be executed
     * @return ProcessComponent|null Process that has been run
     */
    public function generatePrediction(JobEntity $job)
    {
        $processBuilder = $this->_processBuilderFactory->getProcessBuilder();

        $processBuilder->setWorkingDirectory(
            $this->_path->getJobDirectory($job)
        );
        $processBuilder->setPrefix(
            $this->_path->getScriptPath(Path::SCRIPT_GENERATOR)
        );
        $processBuilder->setArguments(
            [
                $this->_path->getScriptsDirectory(),
                $this->_path->getFilePath($job, Path::FILENAME_INPUT),
                $this->_path->getFilePath($job, Path::FILENAME_OUTPUT),
                $this->_path->getFilePath($job, Path::FILENAME_OUTPUT_FILTERED),
                $job->getLength(),
                $job->getGcRatio(),
                $job->getMaxMononucStretches()
            ]
        );
        $processBuilder->setTimeout($this->_jobTimeoutUtil->getTimeoutInSeconds($job));

        $process = $processBuilder->getProcess();
        try {
            $process->run();
        } catch (ProcessTimedOutException $exception) {
            return null;
        }

        return $process;
    }
}
