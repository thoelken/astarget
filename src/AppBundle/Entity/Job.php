<?php declare(strict_types = 1);

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Job
 *
 * @ORM\Table(name="jobs")
 * @ORM\Entity
 */
class Job
{
    /**
     * Email for feedback mail.
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * GC-ration in percent.
     *
     * @var integer
     *
     * @ORM\Column(name="gc_ratio", type="integer", nullable=false)
     */
    private $gcRatio;

    /**
     * Length of sequences.
     *
     * @var integer
     *
     * @ORM\Column(name="length", type="integer", nullable=false)
     */
    private $length;

    /**
     * Job created at timestamp.
     *
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Job status.
     *
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * Job executed at timestamp.
     *
     * @var DateTime
     *
     * @ORM\Column(name="executed_at", type="datetime", nullable=true)
     */
    private $executedAt;

    /**
     * Maximal mononucleotide stretches.
     *
     * @var integer
     *
     * @ORM\Column(name="max_mononuc_stretches", type="integer", nullable=false)
     */
    private $maxMononucStretches;

    /**
     * Length of provided fasta sequence.
     *
     * @var integer
     *
     * @ORM\Column(name="fasta_length", type="integer", nullable=false)
     */
    private $fastaLength;

    /**
     * Job execution started at timestamp.
     *
     * @var DateTime
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * Job timeout in hours.
     *
     * @var integer
     *
     * @ORM\Column(name="timeout", type="integer", nullable=true)
     */
    private $timeout;

    /**
     * Hash value for result page URL.
     *
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    private $hash;

    /**
     * Job ID.
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function setEmail(string $email) : void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function setGcRatio(int $gcRatio) : void
    {
        $this->gcRatio = $gcRatio;
    }

    /**
     * @return int|null
     */
    public function getGcRatio()
    {
        return $this->gcRatio;
    }

    public function setLength(int $length) : void
    {
        $this->length = $length;
    }

    /**
     * @return int|null
     */
    public function getLength()
    {
        return $this->length;
    }

    public function setCreatedAt(DateTime $createdAt) : void
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    public function setStatus(string $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function setExecutedAt(DateTime $executedAt) : void
    {
        $this->executedAt = $executedAt;
    }

    /**
     * @return DateTime|null
     */
    public function getExecutedAt()
    {
        return $this->executedAt;
    }

    public function setMaxMononucStretches(int $maxMononucStretches) : void
    {
        $this->maxMononucStretches = $maxMononucStretches;
    }

    /**
     * @return int|null
     */
    public function getMaxMononucStretches()
    {
        return $this->maxMononucStretches;
    }

    public function setFastaLength(int $fastaLength) : void
    {
        $this->fastaLength = $fastaLength;
    }

    public function getFastaLength() : int
    {
        return $this->fastaLength;
    }

    public function setStartedAt(DateTime $startedAt) : void
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return DateTime|null
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    public function setTimeout(int $timeout) : void
    {
        $this->timeout = $timeout;
    }

    /**
     * @return int|null
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function getId() : int
    {
        return $this->id;
    }
}
