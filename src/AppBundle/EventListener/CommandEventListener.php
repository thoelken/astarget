<?php declare(strict_types = 1);

namespace AppBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;

class CommandEventListener
{
    /**
     * @var LoggerInterface
     */
    private $_logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }

    /**
     * Logs all exceptions thrown in console commands.
     *
     * @param ConsoleExceptionEvent $event
     */
    public function onConsoleException(ConsoleExceptionEvent $event) : void
    {
        $command   = $event->getCommand();
        $exception = $event->getException();

        $message = sprintf(
            '%s: %s (uncaught exception) at %s line %s while running console command `%s`',
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine(),
            $command->getName()
        );

        $this->_logger->error(
            $message,
            ['exception' => $exception]
        );
    }

    /**
     * Logs a warning, if a console command returns an exit code not equal to 0.
     *
     * @param ConsoleTerminateEvent $event
     */
    public function onConsoleTerminate(ConsoleTerminateEvent $event) : void
    {
        $statusCode = $event->getExitCode();
        $command    = $event->getCommand();

        if ($statusCode === 0) {
            return;
        }

        if ($statusCode > 255) {
            $statusCode = 255;
            $event->setExitCode($statusCode);
        }

        $this->_logger->warning(
            sprintf(
                'Command `%s` exited with status code %d',
                $command->getName(),
                $statusCode
            )
        );
    }
}
