# Anti-Sense Target prediction

Anti-Sense TARGET prediction for ncRNAs.

## Installation

### Create parameters.yml

```bash
$ cp app/config/parameters.yml.dist app/config/parameters.yml
```

Update settings if needed.

### Set up cronjobs

Example crontab configuration:

```bash
# m h dom mon dow  command
*/5 *  *   *   *   /usr/bin/php /var/www/astarget/current/bin/console app:execute-job > /dev/null 2>&1
0   2  *   *   *   /usr/bin/php /var/www/astarget/current/bin/console app:clean > /dev/null 2>&1
```

### Composer

#### Download Composer

```bash
$ ./ci/composer-download.sh
```

#### Install Dependencies

```bash
$ APPLICATION_ENV=(prod|test|dev) ./bin/composer --prefer-source install
```

### NPM

```bash
$ npm install
```

### Vagrant

```bash
$ cd vagrant
$ vagrant up
```

### Migrate database

```bash
$ cd vagrant
$ vagrant ssh
$ cd /var/www/astarget
```

#### Get latest migration

```bash
$ php bin/console doctrine:migrations:migrate latest --env=(prod|test|dev)
```

#### Rollback latest migration

```bash
$ php bin/console doctrine:migrations:migrate prev --env=(prod|test|dev)
```

### Build assets for Production

```bash
$ php bin/console assetic:dump --env=prod --no-debug
```

### DNS for development

Add the following line into `/etc/hosts`:

```
192.168.35.10   www.astarget.local   fb.astarget.local
192.168.35.10   test.astarget.local  test.fb.astarget.local
```

## Development

### Check CS

```bash
$ ./bin/composer cs
```

### Fix CS

```bash
$ ./bin/composer cs-fix
```

### Check MD

```bash
$ ./bin/composer md
```

### Execute tests

```bash
$ ./bin/composer test
```

### Check CS, MD and execute tests

```bash
$ ./bin/composer check
```

### Build test coverage

```bash
$ ./bin/composer test-coverage
```

### Execute QUnit tests

```bash
$ npm test
```

Hint: Enable xdebug.

### Execute Behat tests

```bash
$ ./bin/behat
```

### Generate Entities

```bash
$ cd vagrant
$ vagrant ssh
$ cd /var/www/astarget
$ # generate mapping
$ php bin/console doctrine:mapping:import --env=dev AppBundle
$ # change plural entity and mapping names to singular
$ # convert xml mapping to annotion (create entities)
$ php bin/console doctrine:mapping:convert --env=dev --force annotation src/
$ # generate getters and setters
$ php bin/console doctrine:generate:entities --env=dev --no-backup AppBundle
$ # modify tests and ignore MigrationVersions (needed for migration tool)
```

### Load Fixtures

```bash
$ cd vagrant
$ vagrant ssh
$ cd /var/www/astarget
$ php bin/console doctrine:fixtures:load --env=dev
```
