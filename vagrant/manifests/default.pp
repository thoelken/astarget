Apt {
  update => {
    frequency => 'daily'
  }
}

Exec {
  path => [
    "/usr/local/sbin",
    "/usr/local/bin",
    "/usr/sbin",
    "/usr/bin",
    "/sbin",
    "/bin"
  ]
}

$appName       = "astarget"

$user          = "vagrant"
$group         = "www-data"
$directory     = "/var/www/${appName}"
$resourcePath  = "${directory}/vagrant/manifests/resources"

$mysqlServer   = "localhost"
$mysqlDatabase = "astarget"
$mysqlUser     = "root"
$mysqlPassword = "root"

include apt
include system
include httpserver
include dbserver
include dbmigrations
include custom

class system {
  user { "${user}":
    gid    => "${group}",
    groups => ["${user}"]
  }

  host { "localhost":
    ip           => "127.0.0.1",
    host_aliases => [
      "${appName}.local"
    ]
  }
}

class httpserver {
  apt::ppa { "ppa:ondrej/php":
    before  => Class["apt::update"],
    ensure  => present,
    require => Class["apt"]
  }

  package { [
    "apache2",
    "php7.1",
    "php7.1-dom",
    "php7.1-intl",
    "php7.1-mbstring",
    "php7.1-mysql",
    "php7.1-sqlite3",
    "php7.1-xml",
    "php-xdebug"
  ]:
    ensure  => present,
    require => Class["apt::update"],
    notify  => Service['apache2']
  }

  service { "apache2":
    ensure  => "running",
    enable  => "true",
    require => Package["apache2"]
  }

  file { "${directory}":
    ensure => "directory",
    owner  => "${user}",
    group  => "${group}",
    mode   => "0755"
  }

  file { "/etc/apache2/sites-available/${appName}.conf":
    alias   => "vhostconf",
    source  => "${resourcePath}/apache-vhost.conf",
    owner   => "root",
    group   => "root",
    mode    => "0644",
    require => [
      File["${directory}"],
      Package["apache2"]
    ],
    notify => Service['apache2']
  }

  exec { "enable-vhost":
    command => "a2ensite ${appName}",
    unless  => "test -L /etc/apache2/sites-enabled/${appName}.conf",
    require => File["vhostconf"],
    notify  => Service["apache2"]
  }

  exec { "enable-apache-rewrite":
    command => "a2enmod rewrite",
    unless  => "test -L /etc/apache2/mods-enabled/rewrite.load",
    notify  => Service["apache2"],
    require => Package["apache2"]
  }
}

class dbserver {
  file { "/root/.my.cnf":
    ensure  => present,
    owner   => "root",
    group   => "root",
    mode    => "0600",
    content => "[mysql]
      database = ${mysqlDatabase}
      host     = ${mysqlServer}
      password = ${mysqlPassword}
      user     = ${mysqlUser}
    "
  }

  file { "/home/vagrant/.my.cnf":
    ensure  => present,
    owner   => "vagrant",
    group   => "vagrant",
    mode    => "0600",
    content => "[mysql]
      database = ${mysqlDatabase}
      host     = ${mysqlServer}
      password = ${mysqlPassword}
      user     = ${mysqlUser}
    "
  }

  package { [
    "mysql-server"
  ]:
    ensure  => "latest",
    require => Class["apt::update"]
  }

  service { "mysql":
    enable  => true,
    ensure  => running,
    require => Package["mysql-server"]
  }

  exec { "set-mysql-password":
    unless  => "mysqladmin -u${mysqlUser} -p${mysqlPassword} status",
    command => "mysqladmin -u${mysqlUser} password ${mysqlPassword}",
    require => Service["mysql"]
  }
}

class dbmigrations {
  exec { "create-db":
    command => "php ${directory}/bin/console --env=dev doctrine:database:create --if-not-exists",
    require => Exec["set-mysql-password"],
    onlyif  => "test -e ${directory}/bin/console"
  }

  exec { "migrate-db":
    command => "php ${directory}/bin/console --env=dev doctrine:migrations:migrate --no-interaction",
    require => Exec["create-db"],
    onlyif  => "test -e ${directory}/bin/console"
  }

  exec { "load-testdata":
    command => "php ${directory}/bin/console --env=dev doctrine:fixtures:load --no-interaction --purge-with-truncate",
    require => Exec["migrate-db"],
    onlyif  => "test -e ${directory}/bin/console"
  }
}

class custom {
  apt::ppa { "ppa:j-4/vienna-rna":
    before  => Class["apt::update"],
    ensure  => present,
    require => Class["apt"]
  }

  package { "vienna-rna":
    ensure  => "latest",
    require => Class["apt::update"]
  }
}
