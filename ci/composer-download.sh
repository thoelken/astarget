#!/bin/bash -e

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

EXPECTED_SIGNATURE=$(wget https://composer.github.io/installer.sig -O - -q)
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [[ "${EXPECTED_SIGNATURE}" == "${ACTUAL_SIGNATURE}" ]]; then
    mkdir -p bin
    php ./composer-setup.php --install-dir=bin --filename=composer
fi

rm -f ./composer-setup.php
