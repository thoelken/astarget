# CI Docker image

## Log in GitLab Registry

```bash
$ docker login registry.gitlab.com
```

## Build docker image

```bash
$ docker build -t registry.gitlab.com/thoelken/astarget:ci .
```

## Push docker image

```bash
$ docker push registry.gitlab.com/thoelken/astarget:ci
```
