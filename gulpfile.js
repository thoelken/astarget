/*jshint globalstrict: true, node: true, esversion: 6 */
"use strict";

const gulp        = require("gulp");
const jshint      = require("gulp-jshint");
const qunit       = require("gulp-qunit");
const runSequence = require("run-sequence");

const jsFiles = [
    "src/AppBundle/Resources/public/js/**/*.js",
    "tests/QUnit/**/*.js",
    "gulpfile.js",
];

const qunitTestFiles = "tests/QUnit/**/*.html";

gulp.task("lint", function () {
    return gulp
        .src(jsFiles)
        .pipe(jshint())
        .pipe(jshint.reporter("default"))
        .pipe(jshint.reporter("fail"));
});

gulp.task("test", function () {
    return gulp
        .src(qunitTestFiles)
        .pipe(qunit());
});

gulp.task("default", function (done) {
    runSequence("lint", "test", done);
});
