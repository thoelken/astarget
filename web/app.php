<?php declare(strict_types = 1);

use Composer\Autoload\ClassLoader;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

umask(002);

/** @var ClassLoader $loader */
$loader = require(__DIR__ . '/../app/autoload.php');

$environment = getenv('APPLICATION_ENV') ?: 'prod';

$production = $environment === 'prod';

if ($production) {
    include_once(__DIR__ . '/../var/bootstrap.php.cache');
} else {
    Debug::enable();
}

$kernel = new AppKernel($environment, !$production);
if ($production) {
    $kernel->loadClassCache();
}

$request  = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
